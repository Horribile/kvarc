/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc

import cnames.structs.xcb_connection_t
import kotlinx.cinterop.COpaquePointer
import kotlinx.cinterop.CPointer
import platform.posix.*

@ExperimentalUnsignedTypes
class CommonData(
    val semaphore: sem_t,
    var wnd: UInt = UINT32_MAX,
    var centered: Boolean = true,
    fullScreen: Boolean = false,
    var onTheRun: Boolean = true,
    var connection: CPointer<xcb_connection_t>? = null
)

internal data class SharedCommonData(val userdata: COpaquePointer?)