/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc

import cnames.structs.xcb_connection_t
import cnames.structs.xkb_context
import cnames.structs.xkb_keymap
import cnames.structs.xkb_state
import kotlinx.cinterop.*
import platform.posix.strdup
import vulkan.*
import kvarc.utils.DisposableContainer
import kvarc.utils.logError

@ExperimentalUnsignedTypes
class LinuxSurface(private var _fullscreen: Boolean = false) : DisposableContainer() {

    var window: UInt = UINT32_MAX
        private set
    var connection: CPointer<xcb_connection_t>? = null
        private set

    private var _terminate = false
    private val _coreKeyboard: Keyboard = Keyboard()

    private var _width: UShort = 800u
    private var _height: UShort = 600u
    private val _del = arena.allocArray<xcb_atom_tVar>(1)

    fun initialize() {

        setlocale(LC_ALL, "")
        connection = xcb_connect(null, null)

        if (connection == null || xcb_connection_has_error(connection) != 0) {
            throw RuntimeException(
                "Couldn't connect to X server: error code ${if (connection == null) xcb_connection_has_error(
                    connection
                ) else -1}"
            )
        }

        memScoped {

            val kbdEvent = alloc<uint8_tVar>()

            var ret = xkb_x11_setup_xkb_extension(
                connection,
                XKB_X11_MIN_MAJOR_XKB_VERSION,
                XKB_X11_MIN_MINOR_XKB_VERSION,
                XKB_X11_SETUP_XKB_EXTENSION_NO_FLAGS,
                null, null, kbdEvent.ptr, null
            )

            if (ret == 0) {
                throw RuntimeException("Couldn't setup XKB extension")
            }

            val ctx =
                getContext(TestContextFlags.CONTEXT_NO_FLAG) ?: throw RuntimeException("Couldn't create xkb context")

            val kbdDeviceId = xkb_x11_get_core_keyboard_device_id(connection)
            if (kbdDeviceId == -1) {
                xkb_context_unref(ctx)
                throw RuntimeException("Couldn't find core keyboard device")
            }

            ret = initKbd(_coreKeyboard, connection!!, kbdEvent.value, kbdDeviceId, ctx)
            if (ret != 0) {
                xkb_context_unref(ctx)
                throw RuntimeException("Couldn't initialize core keyboard device")
            }

            ret = createWindow()

            if (ret != 0) {
                xkb_context_unref(ctx)
                deinitKbd(_coreKeyboard)
                throw RuntimeException("Couldn't create a capture window")
            }

        }

    }

    fun messageLoop() {
        disableStdinEcho()
        loop(connection!!, _coreKeyboard)
        enableStdinEcho()
    }

    override fun dispose() {
        deinitKbd(_coreKeyboard)
        xcb_disconnect(connection)
        super.dispose()
    }

    private data class Keyboard(
        var conn: CPointer<xcb_connection_t>? = null,
        var first_xkb_event: uint8_t = 0u,
        var ctx: CPointer<xkb_context>? = null,
        var keymap: CPointer<xkb_keymap>? = null,
        var state: CPointer<xkb_state>? = null,
        var device_id: int32_t = 0
    )

    private enum class KeyboardEvents(val value: UInt) {

        REQUIRED_EVENTS(XCB_XKB_EVENT_TYPE_NEW_KEYBOARD_NOTIFY or XCB_XKB_EVENT_TYPE_MAP_NOTIFY or XCB_XKB_EVENT_TYPE_STATE_NOTIFY),
        REQUIRED_NKN_DETAILS(XCB_XKB_NKN_DETAIL_KEYCODES),
        REQUIRED_MAP_PARTS(
            XCB_XKB_MAP_PART_KEY_TYPES or
                    XCB_XKB_MAP_PART_KEY_SYMS or
                    XCB_XKB_MAP_PART_MODIFIER_MAP or
                    XCB_XKB_MAP_PART_EXPLICIT_COMPONENTS or
                    XCB_XKB_MAP_PART_KEY_ACTIONS or
                    XCB_XKB_MAP_PART_VIRTUAL_MODS or
                    XCB_XKB_MAP_PART_VIRTUAL_MOD_MAP
        ),
        REQUIRED_STATE_DETAILS(
            XCB_XKB_STATE_PART_MODIFIER_BASE or
                    XCB_XKB_STATE_PART_MODIFIER_LATCH or
                    XCB_XKB_STATE_PART_MODIFIER_LOCK or
                    XCB_XKB_STATE_PART_GROUP_BASE or
                    XCB_XKB_STATE_PART_GROUP_LATCH or
                    XCB_XKB_STATE_PART_GROUP_LOCK
        )
    }

    private enum class TestContextFlags(val value: Int) {
        CONTEXT_NO_FLAG(0),
        CONTEXT_ALLOW_ENVIRONMENT_NAMES(1 shl 0)
    }

    private fun getContext(test_flags: TestContextFlags): CPointer<xkb_context>? {

        var ctxFlags: xkb_context_flags
        val ctx: CPointer<xkb_context>?

        ctxFlags = XKB_CONTEXT_NO_DEFAULT_INCLUDES
        if ((test_flags.value and TestContextFlags.CONTEXT_ALLOW_ENVIRONMENT_NAMES.value) > 0) {
            unsetenv("XKB_DEFAULT_RULES")
            unsetenv("XKB_DEFAULT_MODEL")
            unsetenv("XKB_DEFAULT_LAYOUT")
            unsetenv("XKB_DEFAULT_VARIANT")
            unsetenv("XKB_DEFAULT_OPTIONS")
        } else {
            ctxFlags = ctxFlags or XKB_CONTEXT_NO_ENVIRONMENT_NAMES
        }

        ctx = xkb_context_new(ctxFlags)
        if (ctx == null) return null

        val path = getPath("") ?: return null

        xkb_context_include_path_append(ctx, path)

        return ctx
    }

    private fun initKbd(
        kbd: Keyboard,
        conn: CPointer<xcb_connection_t>,
        first_xkb_event: uint8_t,
        device_id: int32_t,
        ctx: CPointer<xkb_context>
    ): Int {

        kbd.conn = conn
        kbd.first_xkb_event = first_xkb_event
        kbd.ctx = ctx
        kbd.keymap = null
        kbd.state = null
        kbd.device_id = device_id

        var ret = updateKeymap(kbd)

        if (ret < 0) return -1

        ret = selectXkbEvents(conn, device_id.convert())
        if (ret < 0) {
            xkb_state_unref(kbd.state)
            xkb_keymap_unref(kbd.keymap)
            return -1
        }

        return 0
    }

    private fun deinitKbd(kbd: Keyboard) {
        xkb_state_unref(kbd.state)
        xkb_keymap_unref(kbd.keymap)
    }

    private fun createWindow(): Int {

        val screen = xcb_setup_roots_iterator(xcb_get_setup(connection)).useContents {
            this.data
        }

        val window = xcb_generate_id(connection)

        val values = UIntArray(2)
        values[0] = screen!!.pointed.black_pixel
        values[1] =
            XCB_EVENT_MASK_EXPOSURE or
                    XCB_EVENT_MASK_BUTTON_PRESS or
                    XCB_EVENT_MASK_BUTTON_RELEASE or
                    XCB_EVENT_MASK_POINTER_MOTION or
                    XCB_EVENT_MASK_ENTER_WINDOW or
                    XCB_EVENT_MASK_LEAVE_WINDOW or
                    XCB_EVENT_MASK_KEY_PRESS or
                    XCB_EVENT_MASK_KEY_RELEASE or
                    XCB_EVENT_MASK_STRUCTURE_NOTIFY

        var cookie = xcb_create_window_checked(
            connection, XCB_COPY_FROM_PARENT.convert(), window, screen.pointed.root,
            0, 0, _width, _height, 1, XCB_WINDOW_CLASS_INPUT_OUTPUT.convert(), screen.pointed.root_visual,
            XCB_CW_BACK_PIXEL or XCB_CW_EVENT_MASK, values.refTo(0)
        )

        var error = xcb_request_check(connection, cookie)
        if (error != null) {
            free(error)
            return -1
        }

        val title = "Kvarc"
        xcb_change_property(
            connection,
            XCB_PROP_MODE_REPLACE.convert(),
            window,
            XCB_ATOM_WM_NAME,
            XCB_ATOM_STRING,
            8,
            title.length.convert(),
            title.cstr
        )

        memScoped {

            val protocolsAtom = replyAtom(1.convert(), "WM_PROTOCOLS")
            val deleteWindowAtom = replyAtom(0.convert(), "WM_DELETE_WINDOW")

            _del[0] = deleteWindowAtom!!.pointed.atom

            xcb_change_property(
                connection, XCB_PROP_MODE_REPLACE.convert(),
                window, protocolsAtom!!.pointed.atom,
                4,
                32,
                1,
                _del.getPointer(this)
            )

            free(protocolsAtom)
            free(deleteWindowAtom)

            // TODO switch on the fly with video mode switch
            if (_fullscreen) {

                val stateAtom = replyAtom(0.convert(), "_NET_WM_STATE")
                val fullscreenAtom = replyAtom(0.convert(), "_NET_WM_STATE_FULLSCREEN")

                val fs = allocArray<xcb_atom_tVar>(1)
                fs[0] = fullscreenAtom!!.pointed.atom

                xcb_change_property(
                    connection,
                    XCB_PROP_MODE_REPLACE.convert(),
                    window,
                    stateAtom!!.pointed.atom,
                    XCB_ATOM_ATOM,
                    32,
                    1,
                    fs.getPointer(this)
                )

                free(stateAtom)
                free(fullscreenAtom)
            }

        }

        cookie = xcb_map_window_checked(connection, window)

        error = xcb_request_check(connection, cookie)
        if (error != null) {
            free(error)
            return -1
        }

        xcb_flush(connection)

        return 0
    }

    private fun replyAtom(
        ifExists: UByte,
        str: String
    ): CPointer<xcb_intern_atom_reply_t>? {

        val cookie = xcb_intern_atom(connection, ifExists, str.length.convert(), str)
        return xcb_intern_atom_reply(connection, cookie, null)

    }

    private fun enableStdinEcho() {
        memScoped {
            val termios = alloc<termios>()
            if (tcgetattr(STDIN_FILENO, termios.ptr) == 0) {
                termios.c_lflag = termios.c_lflag or ECHO.toUInt()
                tcsetattr(STDIN_FILENO, TCSADRAIN, termios.ptr)
            }
        }

    }

    private fun disableStdinEcho() {

        memScoped {
            val termios = alloc<termios>()
            if (tcgetattr(STDIN_FILENO, termios.ptr) == 0) {
                termios.c_cflag = termios.c_cflag or ECHO.toUInt().inv()
                tcsetattr(STDIN_FILENO, TCSADRAIN, termios.ptr)
            }
        }

    }

    private fun selectXkbEvents(conn: CPointer<xcb_connection_t>, device_id: uint32_t): Int {

        memScoped {

            val details = alloc<xcb_xkb_select_events_details_t>()

            val cookie = xcb_xkb_select_events_aux_checked(
                conn,
                device_id.convert(),
                KeyboardEvents.REQUIRED_EVENTS.value.convert(),
                0,
                0,
                KeyboardEvents.REQUIRED_MAP_PARTS.value.convert(),
                KeyboardEvents.REQUIRED_MAP_PARTS.value.convert(),
                details.ptr
            )

            val error = xcb_request_check(conn, cookie)

            if (error != null) {
                free(error)
                return -1
            }

            return 0

        }

    }

    private fun updateKeymap(kbd: Keyboard): Int {

        var newState: CPointer<xkb_state>?

        val newKeymap = xkb_x11_keymap_new_from_device(kbd.ctx, kbd.conn, kbd.device_id, 0)
        newKeymap?.let { keymap ->

            newState = xkb_x11_state_new_from_device(newKeymap, kbd.conn, kbd.device_id)

            newState?.let {

            } ?: run {
                xkb_keymap_unref(keymap)
                return -1
            }

            xkb_state_unref(kbd.state)
            xkb_keymap_unref(kbd.keymap)

            kbd.keymap = newKeymap
            kbd.state = newState


        } ?: return -1

        return 0

    }


    private fun processXkbEvent(gevent: CPointer<xcb_generic_event_t>, kbd: Keyboard) {

        @Suppress("UNCHECKED_CAST")
        val event = gevent as CPointer<xkb_event>

        if (event.pointed.any.deviceID != kbd.device_id.toUByte()) return

        /*
        * XkbNewKkdNotify and XkbMapNotify together capture all sorts of keymap
        * updates (e.g. xmodmap, xkbcomp, setxkbmap), with minimal redundent
        * recompilations.
        */
        when (event.pointed.any.xkbType) {
            XCB_XKB_NEW_KEYBOARD_NOTIFY.toUByte() ->
                if ((event.pointed.new_keyboard_notify.changed and XCB_XKB_NKN_DETAIL_KEYCODES.toUShort()) > 0u) {
                    updateKeymap(kbd)
                }
            XCB_XKB_MAP_NOTIFY.toUByte() -> {
                updateKeymap(kbd)
            }
            XCB_XKB_STATE_NOTIFY.toUByte() -> {
                xkb_state_update_mask(
                    kbd.state,
                    event.pointed.state_notify.baseMods.convert(),
                    event.pointed.state_notify.latchedMods.convert(),
                    event.pointed.state_notify.lockedMods.convert(),
                    event.pointed.state_notify.baseGroup.convert(),
                    event.pointed.state_notify.latchedGroup.convert(),
                    event.pointed.state_notify.lockedGroup.convert()
                )
            }

        }

    }

    private fun processEvent(gevent: CPointer<xcb_generic_event_t>, kbd: Keyboard) {

        when (gevent.pointed.response_type) {

            XCB_KEY_PRESS.toUByte() -> {

                @Suppress("UNCHECKED_CAST")
                val event = gevent as CPointer<xcb_key_press_event_t>
                val keycode = event.pointed.detail

                /* Exit on ESC. */
                if (keycode == 9.toUByte())
                    _terminate = true

            }
            XCB_DESTROY_NOTIFY.toUByte() -> {
                _terminate = true
            }
            XCB_CONFIGURE_NOTIFY.toUByte() -> {

                @Suppress("UNCHECKED_CAST")
                val conf = (gevent as CPointer<xcb_configure_notify_event_t>).pointed
                if (conf.width != _width && conf.height != _height) {
                    //TODO listener
                }

            }
            XCB_CLIENT_MESSAGE.toUByte() -> {

                @Suppress("UNCHECKED_CAST")
                val clt = (gevent as CPointer<xcb_client_message_event_t>).pointed

                if (clt.data.data32[0] == _del[0]) {
                    _terminate = true
                }

            }
            else -> {
                if (gevent.pointed.response_type == kbd.first_xkb_event) {
                    processXkbEvent(gevent, kbd)
                }
            }
        }
    }

    private fun loop(conn: CPointer<xcb_connection_t>, kbd: Keyboard) {

        run exit@{

            while (!_terminate) {

                when (xcb_connection_has_error(conn)) {
                    0 -> {
                    }
                    XCB_CONN_ERROR -> {
                        logError("Closed connection to X server: connection error")
                        return@exit
                    }
                    XCB_CONN_CLOSED_EXT_NOTSUPPORTED -> {
                        logError("Closed connection to X server: extension not supported")
                        return@exit
                    }
                    else -> {
                        logError("Closed connection to X server: error code %d")
                        return@exit
                    }

                }

                val event = xcb_poll_for_event(conn)
                event?.let {
                    processEvent(event, kbd)
                    free(event)
                }

            }


        }

    }

    private fun getPath(path_rel: String): String? {

        val srcdir = getenv("top_srcdir")?.toKString() ?: "."
        if (path_rel.length == 1 && path_rel[0] == '/')
            return strdup(path_rel)?.toKString()

        val pathLength = srcdir.length + 12

        val path = ByteArray(pathLength)

        path.usePinned {
            snprintf(it.addressOf(0), pathLength.convert(), "%s/test/data/%s", srcdir, path_rel)
        }

        return path.stringFromUtf8()

    }


}