/*
 * Copyright (C) 2019 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc

import kotlinx.cinterop.*

import kotlin.native.concurrent.DetachedObjectGraph
import kotlin.native.concurrent.attach

import platform.posix.*

import kvarc.global.sharedData
import kvarc.graphics.Renderer
import kvarc.graphics.vk.utils.VK_CHECK
import kvarc.utils.*
import kvarc.utils.PlatformEnum
import platform.posix.pthread_tVar
import vulkan.*

@ExperimentalUnsignedTypes
internal actual object Platform {

    actual fun initialize() = try {

        val arena = Arena()

        val semaphore = arena.alloc<sem_t>()
        sem_init(semaphore.ptr, 0, 0)

        val commonData = CommonData(semaphore)
        val commonDataStableRef = StableRef.create(commonData)

        sharedData.kotlinObject = DetachedObjectGraph {
            SharedCommonData(commonDataStableRef.asCPointer())
        }.asCPointer()

        memScoped {

            val winThread = alloc<pthread_tVar>()

            // It lies about lambda
            platform.posix.pthread_create(winThread.ptr, null, staticCFunction { _: COpaquePointer? ->

                initRuntimeIfNeeded()

                val kotlinObject = DetachedObjectGraph<SharedCommonData>(sharedData.kotlinObject).attach()
                val data = kotlinObject.userdata!!.asStableRef<CommonData>().get()

                val win = LinuxSurface(false)
                win.initialize()

                data.wnd = win.window
                data.connection = win.connection

                sem_post(data.semaphore.ptr)

                win.messageLoop()
                win.dispose()

                null as COpaquePointer? //keep it lies not needed
            }, null)
                .ensureUnixCallResult("pthread_create")

            val vkThread = alloc<pthread_tVar>()

            platform.posix.pthread_create(vkThread.ptr, null, staticCFunction { _: COpaquePointer? ->

                initRuntimeIfNeeded()

                val kotlinObject = DetachedObjectGraph<SharedCommonData>(sharedData.kotlinObject).attach()
                val data = kotlinObject.userdata!!.asStableRef<CommonData>().get()
                sem_wait(data.semaphore.ptr)

                var renderer: Renderer? = null

                try {

                    renderer = Renderer(kotlinObject)
                    renderer.initialize()

                } catch (exc: Exception) {
                    logError(exc.message ?: "Unknown Error")
                }

                renderer?.dispose()

                null as COpaquePointer? //keep it lies not needed

            }, null)
                .ensureUnixCallResult("pthread_create")

            platform.posix.pthread_join(vkThread.value, null).ensureUnixCallResult("pthread_join")
            platform.posix.pthread_join(winThread.value, null).ensureUnixCallResult("pthread_join")

        }

        sem_destroy(semaphore.ptr)
        commonDataStableRef.dispose()

        arena.clear()

    } catch (ex: Exception) {
        logError("Failed to start with exception: ${ex.message}")
    }

    actual val type = PlatformEnum.LINUX
    actual val VK_KHR_PLATFORM_SURFACE_EXTENSION_NAME = VK_KHR_XCB_SURFACE_EXTENSION_NAME

    actual fun createSurface(instance: VkInstance, surfaceVar: VkSurfaceKHRVar, shared: SharedCommonData) {

        memScoped {

            val sharedData = shared.userdata!!.asStableRef<CommonData>().get()

            val surfaceinfo = alloc<VkXcbSurfaceCreateInfoKHR> {
                sType = VK_STRUCTURE_TYPE_XCB_SURFACE_CREATE_INFO_KHR
                pNext = null
                flags = 0u
                window = sharedData.wnd
                connection = sharedData.connection

            }

            if (!VK_CHECK(vkCreateXcbSurfaceKHR(instance, surfaceinfo.ptr, null, surfaceVar.ptr))) {
                throw RuntimeException("Failed to create surface.")
            }


        }

    }

}
