package kvarc.utils

import platform.posix.*

const val TAG = "KVARC"

@ExperimentalUnsignedTypes
internal fun logInfo(message: String) {
    println("Info/$TAG: $message")
}

@ExperimentalUnsignedTypes
internal fun logDebug(message: String) {
    println("Debug/$TAG: $message")
}

@ExperimentalUnsignedTypes
internal fun logError(message: String) {
    println("Error/$TAG: $message")
}

internal inline fun Int.ensureUnixCallResult(op: String, predicate: (Int) -> Boolean = { x -> x == 0 }): Int {
    if (!predicate(this)) {
        throw Error("$op: ${platform.posix.strerror(posix_errno())!!}")
    }
    return this
}