/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc.graphics

import kotlinx.cinterop.*
import kvarc.Platform
import kvarc.SharedCommonData
import kvarc.global.sharedData
import kvarc.graphics.vk.*
import kvarc.graphics.vk.helpers.get
import kvarc.graphics.vk.primitives.UniformBuffers
import kvarc.graphics.vk.primitives.VertexBuffer
import kvarc.graphics.vk.utils.VK_CHECK
import kvarc.utils.DisposableContainer
import kvarc.utils.logError
import platform.posix.CLOCK_MONOTONIC
import platform.posix.clock_gettime
import platform.posix.timespec
import vulkan.*
import kotlin.native.concurrent.DetachedObjectGraph
import kotlin.native.concurrent.attach

@ExperimentalUnsignedTypes
internal class Renderer(private val shared: SharedCommonData) : DisposableContainer() {

    private var _instance: Instance? = null
    private var _surface: VkSurfaceKHRVar = arena.alloc()
    private var _physicalDevice: PhysicalDevice? = null
    private var _logicalDevice: LogicalDevice? = null

    //swapchain
    private var _swapchain: SwapChain? = null
    private var _depthstencil: DepthStencil? = null

    //renderpass
    private var _renderpass: RenderPass? = null

    //pipeline
    private var _uniformBuffers: UniformBuffers? = null
    private var _pipelineCache: PipelineCache? = null
    private var _descriptorSetLayout: DescriptorSetLayout? = null
    private var _pipelineLayout: PipelineLayout? = null
    private var _descriptorPool: DescriptorPool? = null
    private var _descriptorSet: DescriptorSet? = null
    private var _pipeline: Pipeline? = null

    private var _framebuffers: FrameBuffers? = null
    private var _sync: Sync? = null
    private lateinit var _triangle: VertexBuffer
    private var _commandBuffers: CommandBuffers? = null

    private val _counter : timespec = arena.alloc()
    private var _elapsed: Long = 0
    private val _fpsCounter : timespec = arena.alloc()

    private val _kotlinObject = DetachedObjectGraph<SharedCommonData>(sharedData.kotlinObject).attach()

    private var currentBuffer = with(arena) { alloc<uint32_tVar>() }.apply {
        this.value = 0u
    }

    fun initialize() {

        _instance = Instance()

        Platform.createSurface(_instance!!.value!!, _surface, _kotlinObject)

        _physicalDevice = PhysicalDevice(_instance!!.value!!, _surface.value!!)

        _logicalDevice = LogicalDevice(_physicalDevice!!)
        _logicalDevice!!.initialize()

        _triangle = VertexBuffer.Triangle(_physicalDevice!!, _logicalDevice!!)
        _pipelineCache = PipelineCache(_logicalDevice!!)
        _descriptorSetLayout = DescriptorSetLayout(_logicalDevice!!)
        _pipelineLayout = PipelineLayout(_logicalDevice!!, _descriptorSetLayout!!)
        _descriptorPool = DescriptorPool(_logicalDevice!!, 1)

        initSwapchainDepandant()

        clock_gettime(CLOCK_MONOTONIC, _counter.ptr)
        _elapsed = (_counter.tv_sec * 1000000000 + _counter.tv_nsec)/1000000
    }

    private fun initSwapchainDepandant() {
        _swapchain = SwapChain(_physicalDevice!!, _surface.value!!, _logicalDevice!!.device!!)
        _depthstencil = DepthStencil(_physicalDevice!!, _logicalDevice!!, _swapchain!!.width, _swapchain!!.height)
        _renderpass = RenderPass(_logicalDevice!!, _swapchain!!, _physicalDevice!!)
        _uniformBuffers = UniformBuffers(_physicalDevice!!, _logicalDevice!!, _swapchain!!)

        _descriptorSet = DescriptorSet(_logicalDevice!!, _descriptorPool!!, _descriptorSetLayout!!, _uniformBuffers!!)
        _pipeline = Pipeline(_logicalDevice!!, _pipelineLayout!!, _renderpass!!, _pipelineCache!!, _swapchain!!)
        _framebuffers = FrameBuffers(_logicalDevice!!, _renderpass!!, _swapchain!!, _depthstencil!!)
        _sync = Sync(_logicalDevice!!, _swapchain!!)
        _commandBuffers = CommandBuffers(
            _logicalDevice!!, _renderpass!!, _swapchain!!, _framebuffers!!, _pipelineLayout!!,
            _descriptorSet!!, _pipeline!!, _triangle
        )
    }


    fun draw() {

        clock_gettime(CLOCK_MONOTONIC, _fpsCounter.ptr)
        val startFrame = (_fpsCounter.tv_sec * 1000000000 + _fpsCounter.tv_nsec)/1000000

        _uniformBuffers!!.update()

        // Wait forever or error
        if (VK_CHECK(
                vkAcquireNextImageKHR(
                    _logicalDevice!!.device,
                    _swapchain!!.swapchain,
                    UINT64_MAX,
                    _sync!!.presentSemaphore,
                    null,
                    currentBuffer.ptr
                )
            )
        ) {

            // Wait command buffer is completed
            if (VK_CHECK(
                    vkWaitForFences(
                        _logicalDevice!!.device,
                        1u,
                        _sync!!.waitFences[currentBuffer.value.toInt()].ptr,
                        VK_TRUE.toUInt(),
                        UINT64_MAX
                    )
                )
            ) {

                if (VK_CHECK(
                        vkResetFences(
                            _logicalDevice!!.device,
                            1u,
                            _sync!!.waitFences[currentBuffer.value.toInt()].ptr
                        )
                    )
                ) {

                    memScoped {

                        val pipelineStageWaitFlags = alloc<VkPipelineStageFlagsVar>()
                        pipelineStageWaitFlags.value = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT

                        val submitInfo = alloc<VkSubmitInfo>().apply {
                            sType = VK_STRUCTURE_TYPE_SUBMIT_INFO
                            pWaitDstStageMask = pipelineStageWaitFlags.ptr
                            pWaitSemaphores = _sync!!.presentSemaphorePtr
                            waitSemaphoreCount = 1u
                            pSignalSemaphores = _sync!!.renderSemaphorePtr
                            signalSemaphoreCount = 1u
                            commandBufferCount = 1u
                            pCommandBuffers = _commandBuffers!!.drawCmdBuffers[currentBuffer.value.toInt()].ptr
                        }

                        // Submit to the graphics queue
                        if (VK_CHECK(
                                vkQueueSubmit(
                                    _logicalDevice!!.deviceQueue,
                                    1u,
                                    submitInfo.ptr,
                                    _sync!!.waitFences[currentBuffer.value.toInt()].value
                                )
                            )
                        ) {

                            // Present the current buffer to the swap chain
                            val swapchains = allocArray<VkSwapchainKHRVar>(1.toInt()) {
                                this.value = _swapchain!!.swapchain
                            }

                            val presentInfo = alloc<VkPresentInfoKHR>().apply {
                                sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR
                                pNext = null
                                swapchainCount = 1u
                                pSwapchains = swapchains
                                waitSemaphoreCount = 1u
                                pWaitSemaphores = _sync!!.renderSemaphorePtr
                                pImageIndices = currentBuffer.ptr
                            }

                            if (!VK_CHECK(vkQueuePresentKHR(_logicalDevice!!.deviceQueue, presentInfo.ptr))) {
                                logError("Failed present queue")
                            }

                        } else {
                            logError("Failed submit queue")
                        }

                    }

                } else {
                    logError("Failed fences reset")
                }

            } else {
                logError("Failed fences wait")
            }
        } else {
            logError("Failed aquire next image")
        }

        clock_gettime(CLOCK_MONOTONIC, _counter.ptr)
        val tmp = (_counter.tv_sec * 1000000000 + _counter.tv_nsec)/1000000
        if((tmp - _elapsed) >= 500){
            val fps = 1000f/(tmp - startFrame)
            _elapsed = tmp
            println("fps: ${fps}")
        }

    }

    private fun disposeSwapchainDependant() {
        _commandBuffers?.dispose()
        _sync?.dispose()
        _framebuffers?.dispose()
        _pipeline?.dispose()
        _descriptorSet?.dispose()
        _uniformBuffers?.dispose()
        _renderpass?.dispose()
        _depthstencil?.dispose()
        _swapchain?.dispose()
    }

    override fun dispose() {

        disposeSwapchainDependant()
        _descriptorPool?.dispose()
        _pipelineLayout?.dispose()
        _descriptorSetLayout?.dispose()
        _pipelineCache?.dispose()
        _triangle.dispose()
        _logicalDevice?.dispose()
        _physicalDevice?.dispose()
        _instance?.let {
            _surface.value?.let { s ->
                vkDestroySurfaceKHR(it.value, s, null)
            }
            _instance?.dispose()
        }
        super.dispose()

    }

}