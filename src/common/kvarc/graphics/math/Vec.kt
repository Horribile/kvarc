/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc.graphics.math

import platform.posix.*

open class Vec(vararg coordinates: Float) {

    protected var buffer: FloatArray = FloatArray(coordinates.size).apply {
        coordinates.copyInto(this)
    }

    var xyzw: Vec
        get() = Vec(*buffer)
        set(value) {
            buffer.forEachIndexed { index, _ ->
                buffer[index] = value.buffer[index]
            }
        }

    val len by lazy {

        sqrtf(this dot this)

    }

    val size by lazy {
        coordinates.size
    }

    fun normalize() = len.let {
        if (it != 0f)
            for (i in 0 until buffer.size)
                buffer[i] /= it
    }

    open fun normalized() = len.run {

        val copyed = buffer.copyOf()

        if (this != 0f)
            for (i in 0 until copyed.size)
                copyed[i] /= this

        when (buffer.size) {
            2 -> Vec.fromBuffer(*copyed) as Vec2
            3 -> Vec.fromBuffer(*copyed) as Vec3
            4 -> Vec.fromBuffer(*copyed) as Vec4
            else -> throw IllegalArgumentException("not supported vector size")
        }

    }

    fun pointTo(d: Vec, t: Float) = this + d * t

    infix fun dot(vec: Vec): Float = buffer.foldIndexed(0f) { index, sum, element ->
        sum + element * vec.buffer[index]
    }

    open operator fun plus(scalar: Float): Vec = Vec.fromBuffer(*buffer.map {
        it + scalar
    }.toFloatArray())

    open operator fun minus(scalar: Float): Vec = Vec.fromBuffer(*buffer.map {
        it - scalar
    }.toFloatArray())

    open operator fun times(scalar: Float): Vec = Vec.fromBuffer(*buffer.map {
        it * scalar
    }.toFloatArray())

    open operator fun times(v: Vec): Vec {

        val buff = FloatArray(this.size)
        buffer.copyInto(buff)
        buff.forEachIndexed { index, fl ->
            buff[index] = fl * v[index]
        }
        return Vec.fromBuffer(*buff)

    }

    open operator fun div(scalar: Float): Vec = Vec.fromBuffer(
        *buffer.map {
            it / scalar
        }.toFloatArray()
    )

    open operator fun minus(v: Vec): Vec {
        assert(v.buffer.size == this.buffer.size)
        return Vec.fromBuffer(
            *(buffer.mapIndexed { index, it ->
                it - v.buffer[index]
            }.toFloatArray())
        )
    }

    open operator fun plus(v: Vec): Vec {
        assert(v.buffer.size == this.buffer.size)
        return Vec.fromBuffer(
            *buffer.mapIndexed { index, it ->
                it + v.buffer[index]
            }.toFloatArray()
        )
    }

    open operator fun unaryMinus(): Vec = Vec.fromBuffer(
        *buffer.map {
            -it
        }.toFloatArray()
    )

    open operator fun inc() = Vec.fromBuffer(*buffer.map {
        it + 1
    }.toFloatArray())

    open operator fun dec() = Vec.fromBuffer(
        *buffer.map {
            it - 1
        }.toFloatArray()
    )

    operator fun get(index: Int): Float {
        assert(index < buffer.size)
        return buffer[index]
    }

    operator fun set(index: Int, v: Float) {
        assert(index < buffer.size)
        buffer[index] = v
    }

    operator fun set(vararg indices: Int, v: Float) {
        assert(indices.size < buffer.size)
        for (i in indices) {
            assert(i < buffer.size)
            buffer[i] = v
        }
    }

    companion object {

        fun fromBuffer(vararg buffer: Float): Vec {
            when (buffer.size) {
                2 -> return Vec2(buffer[0], buffer[1])
                3 -> return Vec3(buffer[0], buffer[1], buffer[2])
                4 -> return Vec4(buffer[0], buffer[1], buffer[2], buffer[3])
                else -> throw IllegalArgumentException("not supported vector size")
            }

        }

    }

    fun toArray() = floatArrayOf(*buffer)

}

fun radians(degrees: Float): Float {
    return degrees * 0.01745329251994329576923690768489f
}
