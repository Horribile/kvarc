/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc.graphics.math

open class Mat(vararg columns: Vec) {


    private var _size = columns.size

    protected var buffer = FloatArray(_size * _size).apply {
        columns.forEachIndexed { index, vec ->
            vec.toArray().forEachIndexed { idx, fl ->
                this[index * columns.size + idx] = fl
            }
        }
    }

    val size = _size

    operator fun get(column: Int): Vec {
        assert(column < _size)
        return Vec(*buffer.copyOfRange(column * _size, column * _size + _size))
    }

    operator fun get(column: Int, row: Int): Float {
        assert(column < _size && row < _size)
        return buffer[column * _size + row]
    }

    operator fun set(column: Int, v: Vec) {
        assert(column < _size && v.size == _size)
        v.toArray().copyInto(buffer, column * _size, 0, _size - 1)
    }

    operator fun set(column: Int, row: Int, v: Float) {
        assert(column < _size && row < _size)
        buffer[column * _size + row] = v
    }

    operator fun invoke(row: Int, column: Int) = get(column - 1)[row - 1]
    operator fun invoke(row: Int, column: Int, v: Float) = set(column - 1, row - 1, v)

    open operator fun plus(v: Float): Mat {

        val arr = this.toBuffer()
        for (i in 0 until arr.size)
            arr[i] += v

        return when (size) {
            2 -> Mat2.from(*arr)
            3 -> Mat3.from(*arr)
            4 -> Mat4.from(*arr)
            else -> throw IllegalArgumentException("invalid dimensions")
        }
    }

    open operator fun minus(v: Float): Mat {

        val arr = this.toBuffer()
        for (i in 0 until arr.size)
            arr[i] -= v

        return when (size) {
            2 -> Mat2.from(*arr)
            3 -> Mat3.from(*arr)
            4 -> Mat4.from(*arr)
            else -> throw IllegalArgumentException("invalid dimensions")
        }
    }

    open operator fun times(v: Float): Mat {

        val arr = this.toBuffer()
        for (i in 0 until arr.size)
            arr[i] *= v

        return when (size) {
            2 -> Mat2.from(*arr)
            3 -> Mat3.from(*arr)
            4 -> Mat4.from(*arr)
            else -> throw IllegalArgumentException("invalid dimensions")
        }
    }

    open operator fun div(v: Float): Mat {

        val arr = this.toBuffer()
        for (i in 0 until arr.size)
            arr[i] /= v

        return when (size) {
            2 -> Mat2.from(*arr)
            3 -> Mat3.from(*arr)
            4 -> Mat4.from(*arr)
            else -> throw IllegalArgumentException("invalid dimensions")
        }
    }

    open operator fun unaryMinus(): Mat {

        val arr = this.toBuffer()
        for (i in 0 until arr.size)
            arr[i] = -arr[i]

        return when (size) {
            2 -> Mat2.from(*arr)
            3 -> Mat3.from(*arr)
            4 -> Mat4.from(*arr)
            else -> throw IllegalArgumentException("invalid dimensions")
        }
    }

    open operator fun inc(): Mat {

        val arr = this.toBuffer()
        for (i in 0 until arr.size)
            arr[i]++

        return when (size) {
            2 -> Mat2.from(*arr)
            3 -> Mat3.from(*arr)
            4 -> Mat4.from(*arr)
            else -> throw IllegalArgumentException("invalid dimensions")
        }
    }


    open operator fun dec(): Mat {

        val arr = this.toBuffer()
        for (i in 0 until arr.size)
            arr[i]--

        return when (size) {
            2 -> Mat2.from(*arr)
            3 -> Mat3.from(*arr)
            4 -> Mat4.from(*arr)
            else -> throw IllegalArgumentException("invalid dimensions")
        }
    }

    open operator fun plus(m: Mat): Mat {

        val buffer = toBuffer()
        val mbuffer = m.toBuffer()

        for (i in 0 until buffer.size)
            buffer[i] += mbuffer[i]

        return when (size) {
            2 -> Mat2.from(*buffer)
            3 -> Mat3.from(*buffer)
            4 -> Mat4.from(*buffer)
            else -> throw IllegalArgumentException("unsupported dimensions")
        }
    }

    open operator fun minus(m: Mat): Mat {

        val buffer = toBuffer()
        val mbuffer = m.toBuffer()

        for (i in 0 until buffer.size)
            buffer[i] -= mbuffer[i]

        return when (size) {
            2 -> Mat2.from(*buffer)
            3 -> Mat3.from(*buffer)
            4 -> Mat4.from(*buffer)
            else -> throw IllegalArgumentException("unsupported dimensions")
        }
    }


    operator fun times(m: Mat): Mat {

        val transposed = m.transpose().toBuffer()
        val data = ArrayList<Float>()

        buffer.forEachIndexed { index, fl ->
            data.add(fl * transposed[index])
        }

        when (size) {
            2 -> return Mat2.from(*data.toFloatArray())
            3 -> return Mat3.from(*data.toFloatArray())
            4 -> return Mat4.from(*data.toFloatArray())
            else -> throw IllegalArgumentException("Not supported matrix size")
        }

    }

    open fun transpose(): Mat {

        val buff = toBuffer()
        val sz = size

        for (i in 0 until sz) {
            for (j in i + 1 until sz) {
                val tmp = buff[i * sz + j]
                buff[i * sz + j] = buff[j * sz + i]
                buff[j * sz + i] = tmp
            }
        }

        when (size) {
            2 -> return Mat2.from(*buff)
            3 -> return Mat3.from(*buff)
            4 -> return Mat4.from(*buff)
            else -> throw IllegalArgumentException("Not supported matrix size")
        }

    }

    fun determinant(): Float {
        return when (size) {
            2 -> buffer[0] * buffer[2] - buffer[1] * buffer[3]
            3 -> buffer[0] * (buffer[4] * buffer[8] - buffer[7] * buffer[5]) -
                    buffer[3] * (buffer[2] * buffer[8] - buffer[7] * buffer[2]) +
                    buffer[6] * (buffer[1] * buffer[5] - buffer[4] * buffer[2])
            4 -> TODO("Not implemented yet")
            else -> throw IllegalArgumentException("Invalid demensions")
        }
    }

    fun inverse(): Mat {
        TODO("Not implemented")
    }

    fun toBuffer(): FloatArray = buffer.copyOfRange(0, buffer.size)

}
