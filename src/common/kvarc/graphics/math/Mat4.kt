/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc.graphics.math

import platform.posix.*

class Mat4(x: Vec4 = Vec4(x = 1f), y: Vec4 = Vec4(y = 1f), z: Vec4 = Vec4(z = 1f), w: Vec4 = Vec4(w = 1f)) : Mat(x, y, z, w) {

    var X: Vec4
        get() = Vec4(buffer[0], buffer[1], buffer[2], buffer[3])
        set(value) {
            buffer[0] = value[0]
            buffer[1] = value[1]
            buffer[2] = value[2]
            buffer[3] = value[3]
        }

    var Y: Vec4
        get() = Vec4(buffer[4], buffer[5], buffer[6], buffer[7])
        set(value) {
            buffer[4] = value[0]
            buffer[5] = value[1]
            buffer[6] = value[2]
            buffer[7] = value[3]
        }

    var Z: Vec4
        get() = Vec4(buffer[8], buffer[9], buffer[10], buffer[11])
        set(value) {
            buffer[8] = value[0]
            buffer[9] = value[1]
            buffer[10] = value[2]
            buffer[11] = value[3]
        }

    var W: Vec4
        get() = Vec4(buffer[12], buffer[13], buffer[14], buffer[15])
        set(value) {
            buffer[12] = value[0]
            buffer[13] = value[1]
            buffer[14] = value[2]
            buffer[15] = value[3]
        }

    override operator fun inc(): Mat4 = super.inc() as Mat4
    override operator fun dec(): Mat4 = super.dec() as Mat4
    override operator fun unaryMinus(): Mat4 = super.unaryMinus() as Mat4
    override operator fun plus(v: Float): Mat4 = super.plus(v) as Mat4
    override operator fun minus(v: Float): Mat4 = super.minus(v) as Mat4
    override operator fun times(v: Float): Mat4 = super.times(v) as Mat4
    override operator fun div(v: Float): Mat4 = super.div(v) as Mat4
    override operator fun plus(m: Mat): Mat4 = super.plus(m) as Mat4
    override operator fun minus(m: Mat): Mat4 = super.minus(m) as Mat4
    override fun transpose(): Mat4 = super.transpose() as Mat4

    companion object {

        fun from(vararg a: Float): Mat4 {
            assert(a.size == 16)
            return Mat4(Vec4(a[0], a[1], a[2], a[3]), Vec4(a[4], a[5], a[6], a[7]), Vec4(a[8], a[9], a[10], a[11]), Vec4(a[12], a[13], a[14], a[15]))
        }

        val identity = Mat4()

        val ZERO = Mat4.from(*FloatArray(16) { 0f })

        fun lookAt(eye: Vec3, center: Vec3, up: Vec3): Mat4  /*RH*/ {


            val f = (center - eye).normalized()
            val s = (f cross up).normalized()
            val u = s cross f

            val la = Mat4.identity.toBuffer()

            la[0] = s.x
            la[1] = u.x
            la[2] = -f.x

            la[4] = s.y
            la[5] = u.y
            la[6] = -f.y

            la[8] = s.z
            la[9] = u.z
            la[10] = -f.z

            la[12] = -(s dot eye)
            la[13] = -(u dot eye)
            la[14] = -(f dot eye) * -1f //TODO ??? *-1

            return Mat4.from(*la)
        }


        fun perspective(fov: Float, aspect: Float, zNear: Float, zFar: Float): Mat4 {

            assert(fov < 180.0f)
            assert(aspect > 0.001f)

            val tanHalfFovy = tanf(fov / 2f)
//            return Mat4(Vec4(1f / (aspect * tanHalfFovy)), Vec4(y = 1f / tanHalfFovy), Vec4(z = zFar / (zNear - zFar), w = -1f), Vec4(z = -(zNear * zFar) / (zFar - zNear)))
            return Mat4(
                Vec4(1f / (aspect * tanHalfFovy)),
                Vec4(y = 1f / tanHalfFovy),
                Vec4(z = -(zFar + zNear) / (zFar - zNear), w = -1f),
                Vec4(z = -(2f * zNear * zFar) / (zFar - zNear)))

        }

        fun translate(m: Mat4, v: Vec3): Mat4 = Mat4(
            Vec4(1f),
            Vec4(y = 1f),
            Vec4(z = 1f),
            m.X * v.x + m.Y * v.y + m.Z * v.z + m.W
        )


        fun rotate(m: Mat4, angle: Float, v: Vec3): Mat4 {

            val a = angle
            val c = cosf(a)
            val s = sinf(a)

            val axis = v.normalized()
            val temp = axis * (1f - c)

            val Rotate = FloatArray(16)
            Rotate[0] = c + temp[0] * axis[0]
            Rotate[1] = temp[0] * axis[1] + s * axis[2]
            Rotate[2] = temp[0] * axis[2] - s * axis[1]

            Rotate[4] = temp[1] * axis[0] - s * axis[2]
            Rotate[5] = c + temp[1] * axis[1]
            Rotate[6] = temp[1] * axis[2] + s * axis[0]

            Rotate[8] = temp[2] * axis[0] + s * axis[1]
            Rotate[9] = temp[2] * axis[1] - s * axis[0]
            Rotate[10] = c + temp[2] * axis[2]

            val Result = FloatArray(16) { 0f }
            val mm = Mat4.from(*Rotate)


            val x = m.X * mm.X.x + m.Y * mm.X.y + m.Z * mm.X.z
            val y = m.X * mm.Y.x + m.Y * mm.Y.y + m.Z * mm.Y.z
            val z = m.X * mm.Z.x + m.Y * mm.Z.y + m.Z * mm.Z.z
            val w = m.W

            Result[0] = x.x
            Result[1] = x.y
            Result[2] = x.z
            Result[3] = x.w

            Result[4] = y.x
            Result[5] = y.y
            Result[6] = y.z
            Result[7] = y.w

            Result[8] = z.x
            Result[9] = z.y
            Result[10] = z.z
            Result[11] = z.w

            Result[12] = w.x
            Result[13] = w.y
            Result[14] = w.z
            Result[15] = w.w

            return Mat4.from(*Result)

        }
    }
}
