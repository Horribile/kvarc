/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc.graphics.math

class Vec4(x: Float = 0f, y: Float = 0f, z: Float = 0f, w: Float = 0f) : Vec(x, y, z, w) {

    var x: Float
        get() = buffer[0]
        set(value) {
            buffer[0] = value
        }

    var y: Float
        get() = buffer[1]
        set(value) {
            buffer[1] = value
        }

    var z: Float
        get() = buffer[2]
        set(value) {
            buffer[2] = value
        }

    var w: Float
        get() = buffer[3]
        set(value) {
            buffer[3] = value
        }

    override operator fun inc(): Vec4 = super.inc() as Vec4
    override operator fun dec(): Vec4 = super.dec() as Vec4
    override operator fun unaryMinus(): Vec4 = super.unaryMinus() as Vec4
    override operator fun plus(scalar: Float): Vec4 = super.plus(scalar) as Vec4
    override operator fun minus(scalar: Float): Vec4 = super.minus(scalar) as Vec4
    override operator fun times(scalar: Float): Vec4 = super.times(scalar) as Vec4
    override operator fun div(scalar: Float): Vec4 = super.div(scalar) as Vec4
    override operator fun minus(v: Vec): Vec4 = super.minus(v) as Vec4
    override operator fun plus(v: Vec): Vec4 = super.plus(v) as Vec4
    override operator fun times(v: Vec): Vec4 = super.times(v) as Vec4
    override fun normalized(): Vec4 = super.normalized() as Vec4

    companion object {

        val Zero = Vec4()
    }

}
