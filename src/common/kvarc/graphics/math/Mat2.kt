/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc.graphics.math

class Mat2(x: Vec2 = Vec2(x = 1f), y: Vec2 = Vec2(y = 1f)) : Mat(x, y) {

    var X: Vec2
        get() = Vec2(buffer[0], buffer[1])
        set(value) {
            buffer[0] = value[0]
            buffer[1] = value[1]
        }

    var Y: Vec2
        get() = Vec2(buffer[2], buffer[3])
        set(value) {
            buffer[2] = value[0]
            buffer[3] = value[1]
        }

    override operator fun inc(): Mat2 = super.inc() as Mat2
    override operator fun dec(): Mat2 = super.dec() as Mat2
    override operator fun unaryMinus(): Mat2 = super.unaryMinus() as Mat2
    override operator fun plus(v: Float): Mat2 = super.plus(v) as Mat2
    override operator fun minus(v: Float): Mat2 = super.minus(v) as Mat2
    override operator fun times(v: Float): Mat2 = super.times(v) as Mat2
    override operator fun div(v: Float): Mat2 = super.div(v) as Mat2
    override operator fun plus(m: Mat): Mat2 = super.plus(m) as Mat2
    override operator fun minus(m: Mat): Mat2 = super.minus(m) as Mat2
    override fun transpose(): Mat2 = super.transpose() as Mat2

    companion object {

        fun from(vararg a: Float): Mat2 {
            assert(a.size == 4)
            return Mat2(Vec2(a[0], a[1]), Vec2(a[2], a[3]))
        }

        fun identity() = Mat2()
    }
}
