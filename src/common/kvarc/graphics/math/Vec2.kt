/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc.graphics.math

class Vec2(x: Float = 0f, y: Float = 0f) : Vec(x, y) {

    var x: Float
        get() = buffer[0]
        set(value) {
            buffer[0] = value
        }

    var y: Float
        get() = buffer[1]
        set(value) {
            buffer[1] = value
        }

    override operator fun inc(): Vec2 = super.inc() as Vec2
    override operator fun dec(): Vec2 = super.dec() as Vec2
    override operator fun unaryMinus(): Vec2 = super.unaryMinus() as Vec2
    override operator fun plus(scalar: Float): Vec2 = super.plus(scalar) as Vec2
    override operator fun minus(scalar: Float): Vec2 = super.minus(scalar) as Vec2
    override operator fun times(scalar: Float): Vec2 = super.times(scalar) as Vec2
    override operator fun div(scalar: Float): Vec2 = super.div(scalar) as Vec2
    override operator fun minus(v: Vec): Vec2 = super.minus(v) as Vec2
    override operator fun plus(v: Vec): Vec2 = super.plus(v) as Vec2
    override operator fun times(v: Vec): Vec2 = super.times(v) as Vec2
    override fun normalized(): Vec2 = super.normalized() as Vec2

    infix fun cross(rh: Vec2): Vec3 = Vec3(x, y) cross Vec3(rh.x, rh.y)

    companion object {

        val Zero = Vec2()

    }

}
