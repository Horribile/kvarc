/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc.graphics.math

class Mat3(x: Vec3 = Vec3(x = 1f), y: Vec3 = Vec3(y = 1f), z: Vec3 = Vec3(z = 1f)) : Mat(x, y, z) {

    var X: Vec3
        get() = Vec3(buffer[0], buffer[1], buffer[2])
        set(value) {
            buffer[0] = value[0]
            buffer[1] = value[1]
            buffer[2] = value[3]
        }

    var Y: Vec3
        get() = Vec3(buffer[3], buffer[4], buffer[5])
        set(value) {
            buffer[3] = value[0]
            buffer[4] = value[1]
            buffer[5] = value[3]
        }

    var Z: Vec3
        get() = Vec3(buffer[6], buffer[7], buffer[8])
        set(value) {
            buffer[6] = value[0]
            buffer[7] = value[1]
            buffer[8] = value[3]
        }

    override operator fun inc(): Mat3 = super.inc() as Mat3
    override operator fun dec(): Mat3 = super.dec() as Mat3
    override operator fun unaryMinus(): Mat3 = super.unaryMinus() as Mat3
    override operator fun plus(v: Float): Mat3 = super.plus(v) as Mat3
    override operator fun minus(v: Float): Mat3 = super.minus(v) as Mat3
    override operator fun times(v: Float): Mat3 = super.times(v) as Mat3
    override operator fun div(v: Float): Mat3 = super.div(v) as Mat3
    override operator fun plus(m: Mat): Mat3 = super.plus(m) as Mat3
    override operator fun minus(m: Mat): Mat3 = super.minus(m) as Mat3
    override fun transpose(): Mat3 = super.transpose() as Mat3

    companion object {

        fun from(vararg a: Float): Mat3 {
            assert(a.size == 9)
            return Mat3(Vec3(a[0], a[1], a[2]), Vec3(a[3], a[4], a[5]), Vec3(a[6], a[7], a[8]))
        }

        fun identity() = Mat3()
    }
}
