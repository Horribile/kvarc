/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc.graphics.math

class Vec3(x: Float = 0f, y: Float = 0f, z: Float = 0f) : Vec(x, y, z) {

    var x: Float
        get() = buffer[0]
        set(value) {
            buffer[0] = value
        }

    var y: Float
        get() = buffer[1]
        set(value) {
            buffer[1] = value
        }

    var z: Float
        get() = buffer[2]
        set(value) {
            buffer[2] = value
        }

    override operator fun inc(): Vec3 = super.inc() as Vec3
    override operator fun dec(): Vec3 = super.dec() as Vec3
    override operator fun unaryMinus(): Vec3 = super.unaryMinus() as Vec3
    override operator fun plus(scalar: Float): Vec3 = super.plus(scalar) as Vec3
    override operator fun minus(scalar: Float): Vec3 = super.minus(scalar) as Vec3
    override operator fun times(scalar: Float): Vec3 = super.times(scalar) as Vec3
    override operator fun div(scalar: Float): Vec3 = super.div(scalar) as Vec3
    override operator fun minus(v: Vec): Vec3 = super.minus(v) as Vec3
    override operator fun plus(v: Vec): Vec3 = super.plus(v) as Vec3
    override operator fun times(v: Vec): Vec3 = super.times(v) as Vec3
    override fun normalized(): Vec3 = super.normalized() as Vec3

    infix fun cross(rh: Vec3): Vec3 = Vec3(y * rh.z - z * rh.y, -(x * rh.z - z * rh.x), x * rh.y - y * rh.x)

    companion object {
        val Zero = Vec3()
    }

}
