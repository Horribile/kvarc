/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc.graphics.vk

import kotlinx.cinterop.*
import kvarc.graphics.vk.helpers.QueueFamilyIndices
import kvarc.graphics.vk.helpers.VulkanArray
import kvarc.graphics.vk.helpers.forEach
import kvarc.graphics.vk.utils.VK_CHECK
import kvarc.utils.DisposableContainer
import kvarc.utils.logDebug
import vulkan.*

private enum class PoolType {
    GRAPHICS, COMPUTE, TRANSFER
}

@ExperimentalUnsignedTypes
internal class LogicalDevice(
    private val pdevice: PhysicalDevice,
    private val useSwapChain: Boolean = true,
    private val enabledFeatures: VkPhysicalDeviceFeatures? = null,
    private val enabledExtensions: ArrayList<String> = ArrayList(),
    private val requestedQueueTypes: VkQueueFlags = VK_QUEUE_GRAPHICS_BIT,
    private val poolFlags: VkCommandPoolCreateFlags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
    private val presentable: Boolean = true

) : DisposableContainer() {

    private val _device: VkDeviceVar = arena.alloc()
    private val _commandPool: VkCommandPoolVar = with(arena) { alloc() }
    private val _deviceQueue: VkQueueVar = arena.alloc()
    val _queueFamilyIndices: QueueFamilyIndices = QueueFamilyIndices()
    private lateinit var _poolType: PoolType

    private companion object {
        const val DEFAULT_FENCE_TIMEOUT = 100000000000u
    }

    val device by lazy {
        assert(_device.value != null)
        _device.value
    }

    val commandPool by lazy {
        assert(_commandPool.value != null)
        _commandPool.value
    }

    val deviceQueue by lazy {
        assert(_deviceQueue.value != null)
        _deviceQueue.value
    }

    fun createCommandBuffers(
        lvl: VkCommandBufferLevel,
        count: UInt,
        start: Boolean = false
    ): VulkanArray<VkCommandBufferVar> {

        val cmdBuffers: VulkanArray<VkCommandBufferVar> = VulkanArray.Make(count)

        memScoped {

            val commandBufferAllocateInfo = alloc<VkCommandBufferAllocateInfo>().apply {
                sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO
                commandPool = _commandPool.value
                level = lvl
                commandBufferCount = count
            }

            if (!VK_CHECK(vkAllocateCommandBuffers(_device.value, commandBufferAllocateInfo.ptr, cmdBuffers._array)))
                throw RuntimeException("Failed ti create command buffer")

            if (start) {

                val cmdBufferBeginInfo = alloc<VkCommandBufferBeginInfo>().apply {
                    sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO
                }

                cmdBuffers.forEach {
                    if (!VK_CHECK(vkBeginCommandBuffer(it.value, cmdBufferBeginInfo.ptr)))
                        throw RuntimeException("Failed to start command buffer")

                }

            }

            return cmdBuffers

        }

    }

    fun flushCommandBuffer(commandBuffer: VkCommandBufferVar, free: Boolean = true) {


        if (!VK_CHECK(vkEndCommandBuffer(commandBuffer.value)))
            throw RuntimeException("Failed to end command buffer")

        memScoped {

            val submitInfo = alloc<VkSubmitInfo>().apply {
                sType = VK_STRUCTURE_TYPE_SUBMIT_INFO
                commandBufferCount = 1u
                pCommandBuffers = commandBuffer.ptr
            }

            val fenceCreateInfo = alloc<VkFenceCreateInfo>().apply {
                sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO
                flags = 0u
            }

            val fence = alloc<VkFenceVar>()

            if (!VK_CHECK(vkCreateFence(_device.value, fenceCreateInfo.ptr, null, fence.ptr)))
                throw RuntimeException("Failed create fence")

            if (!VK_CHECK(vkQueueSubmit(_deviceQueue.value, 1u, submitInfo.ptr, fence.value)))
                throw RuntimeException("Failed submit queue")

            // Wait for the fence to signal that command buffer has finished executing
            if (!VK_CHECK(
                    vkWaitForFences(
                        _device.value,
                        1u,
                        fence.ptr,
                        VK_TRUE.toUInt(),
                        DEFAULT_FENCE_TIMEOUT.toULong()
                    )
                )
            )
                throw RuntimeException("Fence failed")

            vkDestroyFence(_device.value, fence.value, null)

            if (free) vkFreeCommandBuffers(_device.value, _commandPool.value, 1u, commandBuffer.ptr)

        }

    }

    fun initialize() {

        memScoped {

            val queueCreateInfos: ArrayList<VkDeviceQueueCreateInfo> = ArrayList()
            val defaultQueuePriority = allocArrayOf(1.0f)

            val queueInfo = alloc<VkDeviceQueueCreateInfo>().apply {
                sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO
                queueCount = 1u
                pQueuePriorities = defaultQueuePriority

            }

            if (presentable) {

                val queue = pdevice.presentableQueue

                if (queue < 0)
                    throw RuntimeException("Presentable queue not found")

                _queueFamilyIndices.graphics = queue.toUInt()
                queueInfo.queueFamilyIndex = _queueFamilyIndices.graphics!!
                _poolType = PoolType.GRAPHICS

            } else {

                //TODO for all requested types
                when {
                    ((requestedQueueTypes and VK_QUEUE_GRAPHICS_BIT) > 0u) -> {
                        _queueFamilyIndices.graphics = pdevice.getQueueIndex(VK_QUEUE_GRAPHICS_BIT)
                        queueInfo.queueFamilyIndex = _queueFamilyIndices.graphics!!
                        _poolType = PoolType.GRAPHICS
                    }
                    ((requestedQueueTypes and VK_QUEUE_COMPUTE_BIT) > 0u) -> {

                        _queueFamilyIndices.compute = pdevice.getQueueIndex(VK_QUEUE_COMPUTE_BIT)
                        queueInfo.queueFamilyIndex = _queueFamilyIndices.compute!!
                        _poolType = PoolType.COMPUTE

                    }
                    ((requestedQueueTypes and VK_QUEUE_TRANSFER_BIT) > 0u) -> {
                        _queueFamilyIndices.transfer = pdevice.getQueueIndex(VK_QUEUE_TRANSFER_BIT)
                        queueInfo.queueFamilyIndex = _queueFamilyIndices.transfer!!
                        _poolType = PoolType.TRANSFER
                    }
                }


            }

            queueCreateInfos.add(queueInfo)

            if (useSwapChain && !enabledExtensions.contains(VK_KHR_SWAPCHAIN_EXTENSION_NAME))
                enabledExtensions.add(VK_KHR_SWAPCHAIN_EXTENSION_NAME)

            var idx = 0
            val queueInfos = allocArray<VkDeviceQueueCreateInfo>(queueCreateInfos.size) {
                val info = queueCreateInfos[idx++]
                this.flags = info.flags
                this.sType = info.sType
                this.flags = info.flags
                this.pNext = info.pNext
                this.pQueuePriorities = info.pQueuePriorities
                this.queueCount = info.queueCount
                this.queueFamilyIndex = info.queueFamilyIndex
            }

            val deviceCreateInfo = alloc<VkDeviceCreateInfo>().apply {
                sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO
                queueCreateInfoCount = queueCreateInfos.size.toUInt()
                pQueueCreateInfos = queueInfos
                pEnabledFeatures = enabledFeatures?.ptr
            }

            if (enabledExtensions.size > 0) {
                deviceCreateInfo.enabledExtensionCount = enabledExtensions.size.toUInt()
                deviceCreateInfo.ppEnabledExtensionNames = enabledExtensions.toCStringArray(memScope)
            }

            if (!VK_CHECK(vkCreateDevice(pdevice.device, deviceCreateInfo.ptr, null, _device.ptr)))
                throw RuntimeException("Failed to create _device")
            logDebug("Ok logical device")

            val commandPoolCreateInfo = alloc<VkCommandPoolCreateInfo>().apply {
                sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO
                queueFamilyIndex = when (_poolType) {
                    PoolType.GRAPHICS -> _queueFamilyIndices.graphics!!
                    PoolType.COMPUTE -> _queueFamilyIndices.compute!!
                    PoolType.TRANSFER -> _queueFamilyIndices.transfer!!
                }
                flags = poolFlags
            }

            if (!VK_CHECK(vkCreateCommandPool(_device.value, commandPoolCreateInfo.ptr, null, _commandPool.ptr)))
                throw RuntimeException("Failed to create command pool")
            logDebug("Created command pool ${_commandPool.value.toLong().toString(16)}")

            vkGetDeviceQueue(
                _device.value, when (_poolType) {
                    PoolType.GRAPHICS -> _queueFamilyIndices.graphics!!
                    PoolType.COMPUTE -> _queueFamilyIndices.compute!!
                    PoolType.TRANSFER -> _queueFamilyIndices.transfer!!
                }, 0u, _deviceQueue.ptr
            )

            logDebug("Created device queue ${_deviceQueue.value.toLong().toString(16)}")

        }
    }

    override fun dispose() {

        if (_commandPool.value != null)
            vkDestroyCommandPool(_device.value, _commandPool.value, null)

        if (_device.value != null)
            vkDestroyDevice(_device.value, null)

        super.dispose()
    }


}