/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc.graphics.vk

import kotlinx.cinterop.*
import kvarc.graphics.vk.utils.VK_CHECK
import kvarc.utils.DisposableContainer
import kvarc.utils.logInfo
import vulkan.*

@ExperimentalUnsignedTypes
internal class SwapChain(pDevice: PhysicalDevice, surface: VkSurfaceKHR, private val lDevice: VkDevice) :
    DisposableContainer() {

    val swapchain by lazy {
        assert(_swapchain.value != null)
        _swapchain.value
    }

    var displayFormat: VkFormat = UInt.MAX_VALUE
        private set

    var imageBuffers: ArrayList<Pair<VkImage, VkImageViewVar>> = ArrayList()
        private set

    var width: UInt = 0u
        private set

    var height: UInt = 0u
        private set


    private val _swapchain: VkSwapchainKHRVar = arena.alloc()
    private lateinit var _imagesBuffer: CArrayPointer<VkImageVar>
    private lateinit var _displaySize: VkExtent2D
    private var _presentetionMode: VkPresentModeKHR = VK_PRESENT_MODE_FIFO_KHR
    private var _colorSpace: VkColorSpaceKHR = UInt.MAX_VALUE

    private var vsync = true

    init {

        try {

            val capabilities = pDevice.surfaceCapabilities

            //will be set by swapchain
            if (capabilities.currentExtent.width == (-1).toUInt()) {
                _displaySize.width = width
                _displaySize.height = height
            } else {
                _displaySize = capabilities.currentExtent
                width = _displaySize.width
                height = _displaySize.height
            }

            memScoped {

                logInfo("display size ${_displaySize.width}x${_displaySize.height}")

                val formatsCount = alloc<UIntVar>()
                var result: VkResult

                var buffer: CArrayPointer<VkSurfaceFormatKHR>? = null

                do {

                    result = vkGetPhysicalDeviceSurfaceFormatsKHR(pDevice.device, surface, formatsCount.ptr, null)
                    if (!VK_CHECK(result)) {
                        throw RuntimeException("Could not get surface formats.")
                    }

                    if (formatsCount.value == 0u) break

                    buffer?.let {
                        nativeHeap.free(it)
                        buffer = null
                    }

                    buffer = nativeHeap.allocArray(formatsCount.value.toInt())
                    result =
                        vkGetPhysicalDeviceSurfaceFormatsKHR(
                            pDevice.device,
                            surface,
                            formatsCount.ptr,
                            buffer!!.getPointer(memScope)
                        )
                    if (!VK_CHECK(result)) {
                        throw RuntimeException("Could not get surface formats.")
                    }


                } while (result == VK_INCOMPLETE)

                if (formatsCount.value == 1u) {
                    displayFormat = buffer!![0].format
                    _colorSpace = buffer!![0].colorSpace
                } else {

                    var chosenFormat: UInt? = null

                    for (i in 0u until formatsCount.value) {
                        if (buffer!![i.toInt()].format == VK_FORMAT_R8G8B8A8_UNORM) {
                            chosenFormat = i
                            break
                        }
                    }

                    chosenFormat?.let {
                        displayFormat = buffer!![it.toInt()].format
                        _colorSpace = buffer!![it.toInt()].colorSpace
                    } ?: kotlin.run {
                        displayFormat = buffer!![0].format
                        _colorSpace = buffer!![0].colorSpace
                    }

                }

                nativeHeap.free(buffer!!)

                // The VK_PRESENT_MODE_FIFO_KHR mode must be always
                if (!vsync) {

                    run mail@{

                        pDevice.surfacePresentModes.forEach {

                            if (it == VK_PRESENT_MODE_MAILBOX_KHR) {
                                _presentetionMode = VK_PRESENT_MODE_MAILBOX_KHR
                                return@mail
                            }

                            if ((_presentetionMode != VK_PRESENT_MODE_MAILBOX_KHR) && (it == VK_PRESENT_MODE_IMMEDIATE_KHR))
                                _presentetionMode = VK_PRESENT_MODE_IMMEDIATE_KHR

                        }

                    }

                }

                var imagesNeeded = capabilities.maxImageCount + 1u
                if ((capabilities.maxImageCount > 0u) && (imagesNeeded > capabilities.maxImageCount))
                    imagesNeeded = capabilities.maxImageCount

                val transform: VkSurfaceTransformFlagsKHR =
                    if ((capabilities.supportedTransforms and VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR) > 0u)
                        VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR //won't rotate
                    else
                        capabilities.currentTransform

                // Find a supported composite alpha format
                var compositeAlphaBit = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR
                // Simply select the first composite alpha format available
                val compositeAlphaFlags = arrayOf(
                    VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
                    VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR,
                    VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR,
                    VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR
                )

                kotlin.run found@{
                    compositeAlphaFlags.forEach {
                        if ((capabilities.supportedCompositeAlpha and it) > 0u) {
                            compositeAlphaBit = it
                            return@found
                        }
                    }

                }

                val discardSwapchain = alloc<VkSwapchainKHRVar>()

                val swapchainCreateInfo: VkSwapchainCreateInfoKHR = alloc<VkSwapchainCreateInfoKHR>().apply {
                    sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR
                    pNext = null
                    this.surface = surface
                    minImageCount = imagesNeeded
                    imageFormat = displayFormat
                    imageColorSpace = _colorSpace
                    imageExtent.width = _displaySize.width
                    imageExtent.height = _displaySize.height
                    imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT
                    imageArrayLayers = 1u
                    preTransform = transform
                    queueFamilyIndexCount = 0u
                    pQueueFamilyIndices = null
                    presentMode = _presentetionMode
                    oldSwapchain = discardSwapchain.value
                    // Setting clipped to VK_TRUE allows the implementation to discard rendering outside of the surface area
                    clipped = 1u
                    compositeAlpha = compositeAlphaBit

                }

                //TODO chang for different queues
                if (false) {
                    swapchainCreateInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT
                    swapchainCreateInfo.queueFamilyIndexCount = 2u
                    //swapchainCreateInfo.pQueueFamilyIndices = queueFamilyIndices
                } else {
                    swapchainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE
                }

                // Enable transfer source on swap chain images if supported
                if ((capabilities.supportedUsageFlags and VK_IMAGE_USAGE_TRANSFER_SRC_BIT) > 0u)
                    swapchainCreateInfo.imageUsage = swapchainCreateInfo.imageUsage or VK_IMAGE_USAGE_TRANSFER_SRC_BIT

                // Enable transfer destination on swap chain images if supported
                if ((capabilities.supportedUsageFlags and VK_IMAGE_USAGE_TRANSFER_DST_BIT) > 0u)
                    swapchainCreateInfo.imageUsage = swapchainCreateInfo.imageUsage or VK_IMAGE_USAGE_TRANSFER_DST_BIT

                if (!VK_CHECK(vkCreateSwapchainKHR(lDevice, swapchainCreateInfo.ptr, null, _swapchain.ptr)))
                    throw RuntimeException("Failed to create swapchain")

                if (discardSwapchain.value != null) {

                    imageBuffers.forEach {
                        vkDestroyImageView(lDevice, it.second.value, null)
                    }
                    vkDestroySwapchainKHR(lDevice, discardSwapchain.value, null)
                }

                val imagesCount: UIntVar = alloc()

                if (!VK_CHECK(vkGetSwapchainImagesKHR(lDevice, _swapchain.value, imagesCount.ptr, null)))
                    throw RuntimeException("Failed to initialize vulkan. No images")

                _imagesBuffer = arena.allocArray(imagesCount.value.toInt())

                if (!VK_CHECK(vkGetSwapchainImagesKHR(lDevice, _swapchain.value, imagesCount.ptr, _imagesBuffer)))
                    throw RuntimeException("Failed to initialize vulkan. No images")

                for (i in 0u until imagesCount.value) {

                    val imageViewCreateInfo: VkImageViewCreateInfo = alloc<VkImageViewCreateInfo>().apply {
                        sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO
                        viewType = VK_IMAGE_VIEW_TYPE_2D
                        format = displayFormat
                        components.r = VK_COMPONENT_SWIZZLE_R
                        components.g = VK_COMPONENT_SWIZZLE_G
                        components.b = VK_COMPONENT_SWIZZLE_B
                        components.a = VK_COMPONENT_SWIZZLE_A
                        subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT
                        subresourceRange.baseMipLevel = 0u
                        subresourceRange.levelCount = 1u
                        subresourceRange.baseArrayLayer = 0u
                        subresourceRange.layerCount = 1u
                        pNext = null
                        flags = 0u
                        image = _imagesBuffer[i.toInt()]!!
                    }

                    val imageView = with(arena) { alloc<VkImageViewVar>() }
                    if (!VK_CHECK(vkCreateImageView(lDevice, imageViewCreateInfo.ptr, null, imageView.ptr))) {
                        throw RuntimeException("Failed to create image views")
                    }

                    imageBuffers.add(Pair(_imagesBuffer[i.toInt()]!!, imageView))

                }

            }

        } catch (e: Exception) {
            dispose()
            throw e
        }


    }

    override fun dispose() {
        imageBuffers.forEach {
            vkDestroyImageView(lDevice, it.second.value, null)
        }
        vkDestroySwapchainKHR(lDevice, _swapchain.value, null)
        super.dispose()
    }

}