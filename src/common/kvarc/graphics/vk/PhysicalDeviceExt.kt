/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc.graphics.vk

import kotlinx.cinterop.*
import kvarc.graphics.vk.utils.VK_CHECK
import vulkan.*

@ExperimentalUnsignedTypes
internal val PhysicalDevice.surfacePresentModes: ArrayList<VkPresentModeKHR>
    get() {
        assert(device != null)
        memScoped {

            val modesCount = alloc<UIntVar>()
            var result: VkResult

            var buffer: CArrayPointer<VkPresentModeKHRVar>? = null

            do {

                result = vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, modesCount.ptr, null)
                if (!VK_CHECK(result)) {
                    throw RuntimeException("Could not get surface present modes.")
                }

                if (modesCount.value == 0u) break

                buffer?.let {
                    nativeHeap.free(it)
                    buffer = null
                }

                buffer = nativeHeap.allocArray(modesCount.value.toInt())

                result = vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, modesCount.ptr, buffer)
                if (!VK_CHECK(result)) {
                    throw RuntimeException("Could not get surface present modes.")
                }


            } while (result == VK_INCOMPLETE)

            val modes: ArrayList<VkPresentModeKHR> = ArrayList()
            for (i in 0 until modesCount.value.toInt())
                modes.add(buffer!![i])

            buffer?.let {
                nativeHeap.free(it)
            }

            return modes
        }
    }

@ExperimentalUnsignedTypes
internal val PhysicalDevice.depthFormat: VkFormat
    get() {

        assert(device != null)

        var result: VkFormat? = null

        //TODO check for both support depth/stencil
        // Since all depth formats may be optional, we need to find a suitable depth format to use
        // Start with the highest precision packed format
        val list: ArrayList<VkFormat> = arrayListOf(
            VK_FORMAT_D32_SFLOAT_S8_UINT,
            VK_FORMAT_D32_SFLOAT,
            VK_FORMAT_D24_UNORM_S8_UINT,
            VK_FORMAT_D16_UNORM_S8_UINT,
            VK_FORMAT_D16_UNORM
        )

        kotlin.run found@{

            for (format in list) {

                memScoped {

                    val props: VkFormatProperties = alloc()

                    vkGetPhysicalDeviceFormatProperties(device, format, props.ptr)

                    if ((props.optimalTilingFeatures and VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT) > 0u) {
                        result = format
                        return@found
                    }

                }

            }

        }


        assert(result != null)

        return result!!

    }

/**
 * Get the index of a memory type that has all the requested property bits set
 *
 * @param typeBits Bitmask from VkMemoryRequirements
 * @param properties Bitmask of properties for the memory type to request
 * @param (Optional) memTypeFound Pointer to a bool that is set to true if a matching memory type has been found
 *
 * @return Index of the requested memory type
 *
 * @throw Throws an exception if memTypeFound is null and no memory type could be found that supports the requested properties
 */
@ExperimentalUnsignedTypes
internal fun PhysicalDevice.getMemoryType(
    typeBits: UInt,
    properties: VkMemoryPropertyFlags,
    memTypeFound: VkBool32Var? = null
): UInt {

    var bits = typeBits
    val props = memoryProperties
    for (i in 0u until props.memoryTypeCount) {

        if ((bits and 1u) == 1u) {
            if ((memoryProperties.memoryTypes[i.toInt()].propertyFlags and properties) == properties) {
                if (memTypeFound != null) {
                    memTypeFound.value = 1u
                }
                return i
            }
        }

        bits = bits shr (1)

    }

    if (memTypeFound != null) {
        memTypeFound.value = 0u
        return 0u
    }

    throw RuntimeException("Failed to get memory type")
}
