/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc.graphics.vk

import kotlinx.cinterop.*
import kvarc.Platform.VK_KHR_PLATFORM_SURFACE_EXTENSION_NAME

import vulkan.*

import kvarc.graphics.vk.utils.*
import kvarc.utils.*

@ExperimentalUnsignedTypes
class Instance(val debug: Boolean = true) : DisposableContainer() {

    internal val value by lazy {
        _instance.value
    }

    private var _instance: VkInstanceVar = arena.alloc()
        private set

    private var _msgCallback: VkDebugReportCallbackEXTVar? = null

    init {

        try {

            if (debug) {
                with(arena) { _msgCallback = alloc() }
            }

            memScoped {

                // Getting available extensions
                val availableInstanceExtensions = instanceExtensions

                // If we don't have platform surface extension then there is nothing to do
                if (!instanceExtensions.contains("VK_KHR_surface") || !availableInstanceExtensions.contains(
                        VK_KHR_PLATFORM_SURFACE_EXTENSION_NAME
                    )
                )
                    throw RuntimeException("Needed extensions not supported")

                // Application info
                val appInfo = alloc<VkApplicationInfo>().apply {
                    sType = VK_STRUCTURE_TYPE_APPLICATION_INFO
                    pNext = null
                    apiVersion = VK_MAKE_VERSION(1u, 0u, 0u)
                    applicationVersion = VK_MAKE_VERSION(1u, 0u, 0u)
                    engineVersion = VK_MAKE_VERSION(1u, 0u, 0u)
                    pApplicationName = "kvarc".cstr.ptr
                    pEngineName = "kvarc".cstr.ptr
                    apiVersion = VK_API_VERSION_1_0.toUInt()
                }

                var instanceExt: Array<String> =
                    arrayOf(VK_KHR_SURFACE_EXTENSION_NAME, VK_KHR_PLATFORM_SURFACE_EXTENSION_NAME)

                val debugSupported = availableInstanceExtensions.contains("VK_EXT_debug_report")
                if (debug && debugSupported) instanceExt += "VK_EXT_debug_report"

                // Debug layers will be added a little later if needed
                val instanceCreateInfo = alloc<VkInstanceCreateInfo>().apply {
                    sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO
                    pNext = null
                    pApplicationInfo = appInfo.ptr
                    enabledExtensionCount = instanceExt.size.toUInt()
                    ppEnabledExtensionNames = instanceExt.toCStringArray(memScope)
                    enabledLayerCount = 0u
                    ppEnabledLayerNames = null
                }

                val availableLayers = debugLayers

                if (debug && debugSupported) {
                    instanceCreateInfo.enabledLayerCount = availableLayers.size.toUInt()
                    instanceCreateInfo.ppEnabledLayerNames =
                        availableLayers.toCStringArray(this)
                }

                if (!VK_CHECK(vkCreateInstance(instanceCreateInfo.ptr, null, _instance.ptr)))
                    throw RuntimeException("Failed to create instance")

                if (debug && debugSupported && availableLayers.size > 0) setupLayers(this)


            }

        } catch (e: Exception) {
            dispose()
            throw e
        }
    }


    /**
     * Set up debug layers
     */
    private fun setupLayers(scope: MemScope) {

        with(scope) {

            val dbgCreateInfo = alloc<VkDebugReportCallbackCreateInfoEXT>().apply {

                sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT.toUInt()
                flags = VK_DEBUG_REPORT_INFORMATION_BIT_EXT or VK_DEBUG_REPORT_WARNING_BIT_EXT or
                        VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT or VK_DEBUG_REPORT_ERROR_BIT_EXT or
                        VK_DEBUG_REPORT_DEBUG_BIT_EXT

                pfnCallback = staticCFunction { flags, _, _, _, msgCode, pLayerPrefix, pMsg, _ ->

                    var prefix = "kvarc-"

                    when {

                        flags and VK_DEBUG_REPORT_ERROR_BIT_EXT > 0u -> prefix += "ERROR:"
                        flags and VK_DEBUG_REPORT_WARNING_BIT_EXT > 0u -> prefix += "WARNING:"
                        flags and VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT > 0u -> prefix += "PERFORMANCE:"
                        flags and VK_DEBUG_REPORT_INFORMATION_BIT_EXT > 0u -> prefix += "INFO:"
                        flags and VK_DEBUG_REPORT_DEBUG_BIT_EXT > 0u -> prefix += "DEBUG:"

                    }

                    val debugMessage =
                        "$prefix [${pLayerPrefix?.toKString() ?: ""}] Code $msgCode:${pMsg?.toKString() ?: ""}"

                    if (flags and VK_DEBUG_REPORT_ERROR_BIT_EXT > 0.toUInt()) {
                        logError(debugMessage)
                    } else {
                        logDebug(debugMessage)
                    }

                    // abort/not
                    VK_FALSE.toUInt()
                }

            }

            logInfo("Creating debug callback")
            var kvkCreateDebugReportCallbackEXT: PFN_vkCreateDebugReportCallbackEXT? = null

            vkGetInstanceProcAddr(_instance.value, "vkCreateDebugReportCallbackEXT")?.let { voidFun ->

                @Suppress("UNCHECKED_CAST")
                kvkCreateDebugReportCallbackEXT = voidFun as PFN_vkCreateDebugReportCallbackEXT

            }

            kvkCreateDebugReportCallbackEXT?.let {
                if (!VK_CHECK(
                        it(
                            _instance.value,
                            dbgCreateInfo.ptr,
                            null,
                            _msgCallback?.ptr
                        )
                    )
                ) {
                    logError("Add layers callback failed.")
                }
            } ?: kotlin.run {
                logError("Add layers callback failed.")
            }

        }

    }

    override fun dispose() {

        if (_msgCallback?.value != null) {

            logInfo("Destroying callback")
            var kvkDestroyDebugReportCallbackEXT: PFN_vkDestroyDebugReportCallbackEXT? = null

            vkGetInstanceProcAddr(_instance.value, "vkDestroyDebugReportCallbackEXT")?.let { voidFun ->

                @Suppress("UNCHECKED_CAST")
                kvkDestroyDebugReportCallbackEXT = voidFun as PFN_vkDestroyDebugReportCallbackEXT

            }

            kvkDestroyDebugReportCallbackEXT?.let {

                it(_instance.value, _msgCallback?.value, null)

            }

        }

        vkDestroyInstance(_instance.value, null)

        super.dispose()
    }


}