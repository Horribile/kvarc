/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc.graphics.vk

import kotlinx.cinterop.*
import kvarc.graphics.vk.utils.VK_CHECK
import kvarc.utils.DisposableContainer
import vulkan.*

@ExperimentalUnsignedTypes
internal class DepthStencil(
    private val pDevice: PhysicalDevice,
    private val lDevice: LogicalDevice,
    private val with: UInt,
    private val height: UInt
) : DisposableContainer() {

    class DepthStencilVars(
        var image: VkImageVar? = null,
        var mem: VkDeviceMemoryVar? = null,
        var view: VkImageViewVar? = null
    )

    private var _depthStencil = DepthStencilVars()

    val imageView by lazy {
        _depthStencil.view
    }

    init {

        _depthStencil.image = with(arena) { alloc() }
        _depthStencil.mem = with(arena) { alloc() }
        _depthStencil.view = with(arena) { alloc() }

        memScoped {

            val depthFormat = pDevice.depthFormat
            val imageCreateInfo: VkImageCreateInfo = alloc<VkImageCreateInfo>().apply {

                sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO
                pNext = null
                imageType = VK_IMAGE_TYPE_2D
                format = depthFormat
                extent.width = with
                extent.height = height
                extent.depth = 1u
                mipLevels = 1u
                arrayLayers = 1u
                samples = VK_SAMPLE_COUNT_1_BIT
                tiling = VK_IMAGE_TILING_OPTIMAL
                usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT or VK_IMAGE_USAGE_TRANSFER_SRC_BIT


            }

            val memoryAllocateInfo = alloc<VkMemoryAllocateInfo>().apply {
                sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO
                pNext = null
                allocationSize = 0u
                memoryTypeIndex = 0u
            }

            val imageViewCreateInfo = alloc<VkImageViewCreateInfo>().apply {
                sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO
                pNext = null
                viewType = VK_IMAGE_VIEW_TYPE_2D
                format = depthFormat
                flags = 0u
                subresourceRange.layerCount = 1u
                subresourceRange.aspectMask = (VK_IMAGE_ASPECT_DEPTH_BIT or VK_IMAGE_ASPECT_STENCIL_BIT)
                subresourceRange.baseMipLevel = 0u
                subresourceRange.levelCount = 1u
                subresourceRange.baseArrayLayer = 0u
            }

            val memoryRequirements: VkMemoryRequirements = alloc()

            if (!VK_CHECK(vkCreateImage(lDevice.device, imageCreateInfo.ptr, null, _depthStencil.image!!.ptr)))
                throw RuntimeException("Failed to create image for depthstencil")

            vkGetImageMemoryRequirements(lDevice.device, _depthStencil.image!!.value, memoryRequirements.ptr)

            memoryAllocateInfo.memoryTypeIndex =
                pDevice.getMemoryType(memoryRequirements.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
            memoryAllocateInfo.allocationSize = memoryRequirements.size

            if (!VK_CHECK(vkAllocateMemory(lDevice.device, memoryAllocateInfo.ptr, null, _depthStencil.mem!!.ptr)))
                throw RuntimeException("Failed allocate depthstencil memory")

            if (!VK_CHECK(
                    vkBindImageMemory(
                        lDevice.device,
                        _depthStencil.image!!.value,
                        _depthStencil.mem!!.value,
                        0u
                    )
                )
            )
                throw RuntimeException("Failed allocate depthstencil memory")

            imageViewCreateInfo.image = _depthStencil.image!!.value

            if (!VK_CHECK(vkCreateImageView(lDevice.device, imageViewCreateInfo.ptr, null, _depthStencil.view!!.ptr)))
                throw RuntimeException("Failed create depthstencil imageview")


        }
    }

    override fun dispose() {

        _depthStencil.view?.let {
            vkDestroyImageView(lDevice.device, it.value, null)
        }

        _depthStencil.image?.let {
            vkDestroyImage(lDevice.device, it.value, null)
        }

        _depthStencil.mem?.let {
            vkFreeMemory(lDevice.device, it.value, null)
        }

        super.dispose()
    }

}
