/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc.graphics.vk

import kotlinx.cinterop.*
import kvarc.graphics.vk.utils.VK_CHECK
import kvarc.utils.DisposableContainer
import platform.posix.*
import vulkan.*
import vulkan.uint32_tVar

@ExperimentalUnsignedTypes
internal class PipelineCache(private val lDevice: LogicalDevice) : DisposableContainer() {

    val value by lazy {
        _cache.value
    }

    private val _cache: VkPipelineCacheVar = arena.alloc()
    val _shaderModules = mutableMapOf<String, VkShaderModule>()

    fun loadShader(device: VkDevice, fileName: String): VkShaderModule? {

        if (platform.posix.access(fileName, platform.posix.R_OK) == -1) throw RuntimeException("Cannot read $fileName shader")

        memScoped {

            fun fail(): Nothing = throw Error("Cannot read input file $fileName")

            val size = alloc<stat>().also { if (stat(fileName, it.ptr) != 0) fail() }.st_size
            val file = platform.posix.fopen(fileName, "rb") ?: fail()
            val buf = ByteArray(size.toInt()).also {
                try {
                    fread(it.refTo(0), 1, size.convert(), file)
                } finally {
                    fclose(file)
                }
            }

            //INFO("Loaded file bytes: \r\n ${buf.toHex()}")
            val shaderModule = alloc<VkShaderModuleVar>()

            buf.usePinned {

                val moduleCreateInfo: VkShaderModuleCreateInfo = alloc<VkShaderModuleCreateInfo>().apply {
                    sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO
                    pNext = null
                    codeSize = size.toULong()
                    @Suppress("UNCHECKED_CAST")
                    pCode = it.addressOf(0) as CPointer<uint32_tVar>
                    flags = 0u
                }

                if (!VK_CHECK(vkCreateShaderModule(device, moduleCreateInfo.ptr, null, shaderModule.ptr)))
                    throw RuntimeException("Failed to load shader $fileName")

            }

            _shaderModules.put(fileName, shaderModule.value!!)


        }

        return _shaderModules[fileName]

    }

    init {
        memScoped {

            val pipelineCacheCreateInfo: VkPipelineCacheCreateInfo = alloc<VkPipelineCacheCreateInfo>().apply {
                sType = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO
            }

            if (!VK_CHECK(vkCreatePipelineCache(lDevice.device, pipelineCacheCreateInfo.ptr, null, _cache.ptr)))
                throw RuntimeException("Failed create pipeline cache")

        }
    }

    override fun dispose() {

        vkDestroyPipelineCache(lDevice.device, _cache.value, null)
        super.dispose()
    }

}