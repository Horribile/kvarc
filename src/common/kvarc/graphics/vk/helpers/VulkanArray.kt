/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc.graphics.vk.helpers

import kotlinx.cinterop.CArrayPointer
import kotlinx.cinterop.CVariable
import kotlinx.cinterop.allocArray
import kotlinx.cinterop.get
import kvarc.utils.DisposableContainer

@ExperimentalUnsignedTypes
internal class VulkanArray<T : CVariable>
private constructor(internal val _size: UInt) : DisposableContainer() {

    internal lateinit var _array: CArrayPointer<T>
        private set

    companion object {

        inline fun <reified K : CVariable> Make(size: UInt): VulkanArray<K> {
            val array = VulkanArray<K>(size)
            array._array = with(array.arena) { allocArray(size.toInt()) }
            return array
        }

    }

}

@ExperimentalUnsignedTypes
internal inline operator fun <reified T : CVariable> VulkanArray<T>.iterator(): Iterator<T> {

    return object : Iterator<T> {

        var cursor = 0
        override fun hasNext() = cursor < _size.toInt()
        override fun next(): T = _array.get(cursor++)

    }
}

@ExperimentalUnsignedTypes
internal inline fun <reified T : CVariable> VulkanArray<T>.forEach(callback: (it: T) -> Unit) {

    for (i in 0 until _size.toInt()) {
        callback(_array[i])
    }

}

@ExperimentalUnsignedTypes
internal inline fun <reified T : CVariable> VulkanArray<T>.forEachIndexed(callback: (index: Int, it: T) -> Unit) {

    for (i in 0 until _size.toInt()) {
        callback(i, _array[i])
    }

}

@ExperimentalUnsignedTypes
internal inline operator fun <reified T : CVariable> VulkanArray<T>.get(i: Int): T {
    return _array[i]
}
