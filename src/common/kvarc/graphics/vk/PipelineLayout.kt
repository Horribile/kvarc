/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc.graphics.vk

import kotlinx.cinterop.*
import kvarc.graphics.vk.utils.VK_CHECK
import kvarc.utils.DisposableContainer
import vulkan.*

@ExperimentalUnsignedTypes
internal class PipelineLayout(private val lDevice: LogicalDevice, descSet: DescriptorSetLayout) :
    DisposableContainer() {

    // used by a pipline to access the descriptor sets
    var _pipelineLayout: VkPipelineLayoutVar = with(arena) { alloc() }
        private set

    val value by lazy {
        _pipelineLayout.value
    }

    init {

        memScoped {

            val pipelineLayoutCreateInfo = alloc<VkPipelineLayoutCreateInfo>().apply {
                sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO
                pNext = null
                setLayoutCount = 1u
                pSetLayouts = descSet._descriptorSetLayout.ptr
            }

            if (!VK_CHECK(
                    vkCreatePipelineLayout(
                        lDevice.device,
                        pipelineLayoutCreateInfo.ptr,
                        null,
                        _pipelineLayout.ptr
                    )
                )
            )
                throw RuntimeException("failed create pipeline layout")


        }

    }


    override fun dispose() {
        vkDestroyPipelineLayout(lDevice.device, _pipelineLayout.value, null)
        super.dispose()
    }

}