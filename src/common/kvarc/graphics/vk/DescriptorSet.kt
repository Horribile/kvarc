/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc.graphics.vk

import kotlinx.cinterop.*
import kvarc.graphics.vk.primitives.UniformBuffers
import kvarc.graphics.vk.utils.VK_CHECK
import kvarc.utils.DisposableContainer
import vulkan.*

@ExperimentalUnsignedTypes
internal class DescriptorSet(
    private val device: LogicalDevice,
    private val _pool: DescriptorPool,
    private val _layout: DescriptorSetLayout,
    private val _uniform: UniformBuffers
) : DisposableContainer() {

    private val _descriptorSet: VkDescriptorSetVar = with(arena) { alloc() }

    val value by lazy {
        _descriptorSet.value
    }

    val ptr by lazy {
        _descriptorSet.ptr
    }

    init {
        memScoped {

            val descriptorSetAllocateInfo = alloc<VkDescriptorSetAllocateInfo>().apply {
                sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO
                descriptorPool = _pool.value
                descriptorSetCount = 1u
                pSetLayouts = _layout._descriptorSetLayout.ptr
            }

            // Binding 0 : Uniform buffer
            if (!VK_CHECK(vkAllocateDescriptorSets(device.device, descriptorSetAllocateInfo.ptr, _descriptorSet.ptr)))
                throw RuntimeException("Failed create descriptor set")

            // Update the descriptor set determining the shader binding points

            val writeDescriptorSet = alloc<VkWriteDescriptorSet>().apply {
                sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET
                dstSet = _descriptorSet.value
                descriptorCount = 1u
                descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER
                pBufferInfo = _uniform.uniformBufferVS.descriptor!!.ptr
                dstBinding = 0u
            }

            vkUpdateDescriptorSets(device.device, 1u, writeDescriptorSet.ptr, 0u, null)

        }
    }

    override fun dispose() {
        vkFreeDescriptorSets(device.device, _pool.value, 1u, _descriptorSet.ptr)
        super.dispose()
    }


}