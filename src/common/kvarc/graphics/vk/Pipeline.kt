/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc.graphics.vk

import kotlinx.cinterop.*
import kvarc.graphics.vk.primitives.Vertex
import kvarc.graphics.vk.utils.VK_CHECK
import kvarc.utils.DisposableContainer
import platform.posix.F_OK
import platform.posix.access
import platform.posix.getcwd
import vulkan.VK_SHADER_STAGE_VERTEX_BIT
import vulkan.*

// Pipeline with states that affect a pipeline

@ExperimentalUnsignedTypes
internal class Pipeline(
    private val _device: LogicalDevice,
    private val _pipelineLayout: PipelineLayout,
    private val _renderPass: RenderPass,
    private val _pipelineCache: PipelineCache,
    private val _swapchain: SwapChain
) : DisposableContainer() {

    var _pipeline: VkPipelineVar = with(arena) { alloc() }
        private set

    val value by lazy {
        _pipeline.value
    }

    init {

        memScoped {

            // Shaders

            val shaderStages = allocArray<VkPipelineShaderStageCreateInfo>(2)

            val cwd = ByteArray(1024)
            cwd.usePinned {
                getcwd(it.addressOf(0), 1024)
            }

            var shaderFile = "${cwd.stringFromUtf8()}/assets/shaders/triangle.vert.spv"
            if(access(shaderFile, F_OK) == -1){
                shaderFile = "${cwd.stringFromUtf8()}/build/bin/mingw/mainDebugExecutable/assets/shaders/triangle.vert.spv"
                if(access(shaderFile, F_OK) == -1){
                    shaderFile = "${cwd.stringFromUtf8()}/build/bin/linux/mainDebugExecutable/assets/shaders/triangle.vert.spv"
                }
            }

            // Vertex shader
            shaderStages[0].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO
            shaderStages[0].stage = VK_SHADER_STAGE_VERTEX_BIT
            shaderStages[0].module = _pipelineCache.loadShader(_device.device!!, shaderFile)
            shaderStages[0].pName = "main".cstr.ptr
            assert(shaderStages[0].module != null)

            shaderFile = "${cwd.stringFromUtf8()}/assets/shaders/triangle.frag.spv"
            if(access(shaderFile, F_OK) == -1){
                shaderFile = "${cwd.stringFromUtf8()}/build/bin/mingw/mainDebugExecutable/assets/shaders/triangle.frag.spv"
                if(access(shaderFile, F_OK) == -1){
                    shaderFile = "${cwd.stringFromUtf8()}/build/bin/linux/mainDebugExecutable/assets/shaders/triangle.frag.spv"
                }
            }

            // Fragment shader
            shaderStages[1].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO
            shaderStages[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT
            shaderStages[1].module = _pipelineCache.loadShader(_device.device!!,shaderFile)
            shaderStages[1].pName = "main".cstr.ptr
            assert(shaderStages[1].module != null)


            // Vertex input descriptions

            val vertexInputBindingDescription = Vertex.bindingDescription(this)
            val vertexInputAttributs = Vertex.inputAtributes(this)

            // Vertex input state

            val pipelineVertexInputStateCreateInfo = alloc<VkPipelineVertexInputStateCreateInfo>().apply {
                sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO
                vertexBindingDescriptionCount = 1u
                pVertexBindingDescriptions = vertexInputBindingDescription.ptr
                vertexAttributeDescriptionCount = 2u
                pVertexAttributeDescriptions = vertexInputAttributs
            }

            // Input assembly state

            val pipelineInputAssemblyStateCreateInfo = alloc<VkPipelineInputAssemblyStateCreateInfo>().apply {
                sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO
                topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST
                primitiveRestartEnable = 0u
            }

            // Viewport state

            val viewport = alloc<VkViewport>().apply {
                x = 0.0f
                y = 0.0f
                width = _swapchain.width.toInt().toFloat()
                height = _swapchain.height.toInt().toFloat()
                minDepth = 0f
                maxDepth = 1f
            }

            val scissor = alloc<VkRect2D>().apply {
                offset.x = 0
                offset.y = 0
                extent.width = _swapchain.width
                extent.height = _swapchain.height
            }

            val pipelineViewportStateCreateInfo = alloc<VkPipelineViewportStateCreateInfo>().apply {
                sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO
                viewportCount = 1u
                scissorCount = 1u
                pViewports = viewport.ptr
                pScissors = scissor.ptr
            }

            // Rasterization state

            val pipelineRasterizationStateCreateInfo = alloc<VkPipelineRasterizationStateCreateInfo>().apply {
                sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO
                depthClampEnable = VK_FALSE.toUInt()
                rasterizerDiscardEnable = VK_FALSE.toUInt()
                polygonMode = VK_POLYGON_MODE_FILL
                cullMode = VK_CULL_MODE_BACK_BIT //VK_CULL_MODE_NONE
                lineWidth = 1.0f
                frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE
                depthBiasEnable = VK_FALSE.toUInt()
            }

            // Multi sampling state

            val pipelineMultisampleStateCreateInfo = alloc<VkPipelineMultisampleStateCreateInfo>().apply {
                sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO
                rasterizationSamples = VK_SAMPLE_COUNT_1_BIT
                pSampleMask = null
                sampleShadingEnable = 0u
            }

            // Color blend state

            val blendAttachmentState = allocArray<VkPipelineColorBlendAttachmentState>(1)
            blendAttachmentState[0].apply {
                colorWriteMask = VK_COLOR_COMPONENT_R_BIT or VK_COLOR_COMPONENT_G_BIT or VK_COLOR_COMPONENT_B_BIT or
                        VK_COLOR_COMPONENT_A_BIT //0xfu
                blendEnable = VK_FALSE.toUInt()
            }

            val pipelineColorBlendStateCreateInfo = alloc<VkPipelineColorBlendStateCreateInfo>().apply {
                sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO
                attachmentCount = 1u
                pAttachments = blendAttachmentState
                logicOpEnable = 0u
                logicOp = VK_LOGIC_OP_COPY
                blendConstants[0] = 0.0f
                blendConstants[1] = 0.0f
                blendConstants[2] = 0.0f
                blendConstants[3] = 0.0f
            }


            val pipelineCreateInfo = alloc<VkGraphicsPipelineCreateInfo>().apply {
                sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO
                layout = _pipelineLayout._pipelineLayout.value
                renderPass = _renderPass.renderPass.value
            }


            // Enable dynamic states

            val dynamicStateEnables = allocArray<VkDynamicStateVar>(2)
            dynamicStateEnables[0] = VK_DYNAMIC_STATE_VIEWPORT
            dynamicStateEnables[1] = VK_DYNAMIC_STATE_SCISSOR

            val pipelineDynamicStateCreateInfo = alloc<VkPipelineDynamicStateCreateInfo>().apply {
                sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO
                pDynamicStates = dynamicStateEnables
                dynamicStateCount = 2u
            }

            // Depth and stencil state

            val pipelineDepthStencilStateCreateInfo = alloc<VkPipelineDepthStencilStateCreateInfo>().apply {
                sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO
                depthTestEnable = VK_TRUE.toUInt()
                depthWriteEnable = VK_TRUE.toUInt()
                depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL
                depthBoundsTestEnable = VK_FALSE.toUInt()
                back.failOp = VK_STENCIL_OP_KEEP
                back.passOp = VK_STENCIL_OP_KEEP
                back.compareOp = VK_COMPARE_OP_ALWAYS
                stencilTestEnable = VK_FALSE.toUInt()
                front.failOp = VK_STENCIL_OP_KEEP
                front.passOp = VK_STENCIL_OP_KEEP
                front.compareOp = VK_COMPARE_OP_ALWAYS
            }

            pipelineCreateInfo.stageCount = 2u
            pipelineCreateInfo.pStages = shaderStages

            pipelineCreateInfo.pVertexInputState = pipelineVertexInputStateCreateInfo.ptr
            pipelineCreateInfo.pInputAssemblyState = pipelineInputAssemblyStateCreateInfo.ptr
            pipelineCreateInfo.pRasterizationState = pipelineRasterizationStateCreateInfo.ptr
            pipelineCreateInfo.pColorBlendState = pipelineColorBlendStateCreateInfo.ptr
            pipelineCreateInfo.pMultisampleState = pipelineMultisampleStateCreateInfo.ptr
            pipelineCreateInfo.pViewportState = pipelineViewportStateCreateInfo.ptr
            pipelineCreateInfo.pDepthStencilState = pipelineDepthStencilStateCreateInfo.ptr
            pipelineCreateInfo.renderPass = _renderPass.renderPass.value
            pipelineCreateInfo.pDynamicState = pipelineDynamicStateCreateInfo.ptr

            // Create rendering pipeline using the specified states

            if (!VK_CHECK(
                    vkCreateGraphicsPipelines(
                        _device.device,
                        _pipelineCache.value,
                        1u,
                        pipelineCreateInfo.ptr,
                        null,
                        _pipeline.ptr
                    )
                )
            )
                throw RuntimeException("failed create pipeline")

            vkDestroyShaderModule(_device.device, shaderStages[0].module, null)
            vkDestroyShaderModule(_device.device, shaderStages[1].module, null)

        }

    }

    override fun dispose() {
        vkDestroyPipeline(_device.device, _pipeline.value, null)
        super.dispose()
    }


}