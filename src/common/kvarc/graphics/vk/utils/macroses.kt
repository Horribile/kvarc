/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc.graphics.vk.utils

import kvarc.utils.logDebug
import kvarc.utils.logError
import kvarc.utils.logInfo
import vulkan.*

@ExperimentalUnsignedTypes
fun VK_CHECK(result: VkResult): Boolean {

    when (result) {
        VK_SUCCESS -> return true
        VK_NOT_READY -> {
            logInfo("A fence or query has not yet completed.")
            return true
        }
        VK_TIMEOUT -> {
            logInfo("A wait operation has not completed in the specified time.")
            return true
        }
        VK_EVENT_SET -> {
            logInfo("An event is signaled.")
            return true
        }
        VK_EVENT_RESET -> {
            logInfo("An event is unsignaled.")
            return true
        }
        VK_INCOMPLETE -> {
            logInfo("A return array was too small for the result.")
            return true
        }
        VK_SUBOPTIMAL_KHR -> {
            logInfo("A swapchain no longer matches the surface properties exactly, but can still be used to present to the surface successfully.")
            return true
        }
        VK_ERROR_OUT_OF_HOST_MEMORY -> {
            logError("A host memory allocation has failed.")
            return false
        }
        VK_ERROR_OUT_OF_DEVICE_MEMORY -> {
            logError("A _device memory allocation has failed.")
            return false
        }
        VK_ERROR_INITIALIZATION_FAILED -> {
            logError("Initialization of an object could not be completed for implementation-specific reasons.")
            return false
        }
        VK_ERROR_DEVICE_LOST -> {
            logError("The logical or physical _device has been lost.")
            return false
        }
        VK_ERROR_MEMORY_MAP_FAILED -> {
            logError("Mapping of a memory object has failed.")
            return false
        }
        VK_ERROR_EXTENSION_NOT_PRESENT -> {
            logError("A requested extension is not supported.")
            return false
        }
        VK_ERROR_FEATURE_NOT_PRESENT -> {
            logError("A requested feature is not supported.")
            return false
        }
        VK_ERROR_INCOMPATIBLE_DRIVER -> {
            logError("The requested version of Vulkan is not supported by the driver or is otherwise incompatible for implementation-specific reasons.")
            return false
        }
        VK_ERROR_TOO_MANY_OBJECTS -> {
            logError("Too many objects of the type have already been created.")
            return false
        }
        VK_ERROR_FORMAT_NOT_SUPPORTED -> {
            logError("A requested format is not supported on this _device.")
            return false
        }
        VK_ERROR_FRAGMENTED_POOL -> {
            logError("A pool allocation has failed due to fragmentation of the pool’s memory. This must only be returned if no attempt to allocate host or _device memory was made to accomodate the new allocation. This should be returned in preference to VK_ERROR_OUT_OF_POOL_MEMORY, but only if the implementation is certain that the pool allocation failure was due to fragmentation.")
            return false
        }
        VK_ERROR_SURFACE_LOST_KHR -> {
            logError("A surface is no longer available.")
            return false
        }
        VK_ERROR_NATIVE_WINDOW_IN_USE_KHR -> {
            logError("The requested window is already in use by Vulkan or another API in a manner which prevents it from being used again.")
            return false
        }
        VK_ERROR_OUT_OF_DATE_KHR -> {
            logError("A surface has changed in such a way that it is no longer compatible with the swapchain, and further presentation requests using the swapchain will fail. Applications must query the new surface properties and recreate their swapchain if they wish to continue presenting to the surface.")
            return false
        }
        VK_ERROR_INCOMPATIBLE_DISPLAY_KHR -> {
            logError("The display used by a swapchain does not use the same presentable image layout, or is incompatible in a way that prevents sharing an image.")
            return false
        }
        VK_ERROR_INVALID_SHADER_NV -> {
            logError("One or more shaders failed to compile or link.")
            return false
        }
        VK_ERROR_OUT_OF_POOL_MEMORY -> {
            logError("A pool memory allocation has failed. This must only be returned if no attempt to allocate host or _device memory was made to accomodate the new allocation. If the failure was definitely due to fragmentation of the pool, VK_ERROR_FRAGMENTED_POOL should be returned instead.")
            return false
        }
        VK_ERROR_INVALID_EXTERNAL_HANDLE -> {
            logError("An external handle is not a valid handle of the specified type.")
            return false
        }
        VK_ERROR_FRAGMENTATION_EXT -> {
            logError("A descriptor pool creation has failed due to fragmentation.")
            return false
        }
        VK_ERROR_LAYER_NOT_PRESENT -> {
            logError("Layer not present.")
            return false
        }
        VK_ERROR_VALIDATION_FAILED_EXT -> {
            logError("Validation failed.")
            return false
        }
        VK_ERROR_NOT_PERMITTED_EXT -> {
            logError("Not Permitted.")
            return false
        }
        VK_ERROR_OUT_OF_POOL_MEMORY_KHR -> {
            logError("Out of pool memory.")
            return false
        }
        VK_ERROR_INVALID_EXTERNAL_HANDLE_KHR -> {
            logError("Invalid external handle.")
            return false
        }
        else -> {
            logError("Unknown error. ${result}")
            return false
        }
    }

}


@ExperimentalUnsignedTypes
fun VK_MAKE_VERSION(major: UInt, minor: UInt, patch: UInt): UInt {
    return ((major shl 22) or (minor shl 12) or patch)
}

@ExperimentalUnsignedTypes
fun VK_VERSION_MAJOR(version: UInt): UInt {
    return (version shr 22)
}

@ExperimentalUnsignedTypes
fun VK_VERSION_MINOR(version: UInt): UInt {
    return ((version shr 12) and (0x3ff).toUInt())
}

@ExperimentalUnsignedTypes
fun VK_VERSION_PATCH(version: UInt): UInt {
    return (version and (0xfff).toUInt())
}
