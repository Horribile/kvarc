/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc.graphics.vk

import kotlinx.cinterop.*
import kvarc.graphics.vk.helpers.VulkanArray
import kvarc.graphics.vk.primitives.VertexBuffer
import kvarc.graphics.vk.utils.VK_CHECK
import kvarc.utils.DisposableContainer
import vulkan.*

@ExperimentalUnsignedTypes
internal class CommandBuffers(
    private val _device: LogicalDevice,
    private val _renderpass: RenderPass,
    private val _swapchain: SwapChain,
    private val frameBuffers: FrameBuffers,
    private val _pipelineLayout: PipelineLayout,
    private val _descriptorSet: DescriptorSet,
    private val _pipeline: Pipeline,
    private val _vertices: VertexBuffer
) : DisposableContainer() {

    internal var drawCmdBuffers: VulkanArray<VkCommandBufferVar> =
        VulkanArray.Make(_swapchain.imageBuffers.size.toUInt())
        private set

    init {

        memScoped {

            val cmdBufAllocateInfo = alloc<VkCommandBufferAllocateInfo>().apply {
                sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO
                commandPool = _device.commandPool
                level = VK_COMMAND_BUFFER_LEVEL_PRIMARY
                commandBufferCount = _swapchain.imageBuffers.size.toUInt()
            }

            if (!VK_CHECK(vkAllocateCommandBuffers(_device.device, cmdBufAllocateInfo.ptr, drawCmdBuffers._array)))
                throw RuntimeException("Failed allocate draw command buffers")

            val cmdBufInfo: VkCommandBufferBeginInfo = alloc<VkCommandBufferBeginInfo>().apply {
                sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO
                pNext = null
            }

            // Set clear values for all framebuffer attachments with loadOp set to clear
            // We use two attachments (color and depth) that are cleared at the start of the subpass and as such we need to set clear values for both

            val clearValues = allocArray<VkClearValue>(2)
            clearValues[0].color.float32.apply {
                this[0] = 0.0f
                this[1] = 0.0f
                this[2] = 0.2f
                this[3] = 1.0f
            }

            clearValues[1].depthStencil.apply {
                depth = 1.0f
                stencil = 0u
            }

            val renderPassBeginInfo = alloc<VkRenderPassBeginInfo>().apply {
                sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO
                pNext = null
                renderPass = _renderpass.renderPass.value
                renderArea.offset.x = 0
                renderArea.offset.y = 0
                renderArea.extent.width = _swapchain.width
                renderArea.extent.height = _swapchain.height
                clearValueCount = 2u
                pClearValues = clearValues
            }

            for (i in 0 until drawCmdBuffers._size.toInt()) {

                renderPassBeginInfo.framebuffer = frameBuffers.frameBuffers[i].value

                if (!VK_CHECK(vkBeginCommandBuffer(drawCmdBuffers._array[i], cmdBufInfo.ptr)))
                    throw  RuntimeException("failed begin draw command buffer")

                // Start the first sub pass specified in our default render pass setup by the base class
                // This will clear the color and depth attachment

                vkCmdBeginRenderPass(drawCmdBuffers._array[i], renderPassBeginInfo.ptr, VK_SUBPASS_CONTENTS_INLINE)

                // Update dynamic vp state
                val viewport = alloc<VkViewport>().apply {
                    height = _swapchain.height.toInt().toFloat()
                    width = _swapchain.width.toInt().toFloat()
                    minDepth = 0f
                    maxDepth = 1f
                }

                // just one viewport
                vkCmdSetViewport(drawCmdBuffers._array[i], 0u, 1u, viewport.ptr)

                // Update dynamic scissor state
                val scissor = alloc<VkRect2D>().apply {
                    extent.width = _swapchain.width
                    extent.height = _swapchain.height
                    offset.x = 0
                    offset.y = 0
                }

                // just one scissor
                vkCmdSetScissor(drawCmdBuffers._array[i], 0u, 1u, scissor.ptr)

                //Descriptors for shader binding points
                // just one descriptor set
                vkCmdBindDescriptorSets(
                    drawCmdBuffers._array[i],
                    VK_PIPELINE_BIND_POINT_GRAPHICS,
                    _pipelineLayout._pipelineLayout.value,
                    0u,
                    1u,
                    _descriptorSet.ptr,
                    0u,
                    null
                )

                // The pipeline (state object) contains all states of the rendering pipeline,
                // binding it will set all the states specified at pipeline creation time

                vkCmdBindPipeline(
                    drawCmdBuffers._array[i],
                    VK_PIPELINE_BIND_POINT_GRAPHICS,
                    _pipeline._pipeline.value
                )

                // Bind triangle vertex buffer (contains position and colors)
                val offsets = allocArray<VkDeviceSizeVar>(1) { index: Int ->
                    this.value = 0u
                }

                // Bind vertex and index buffers
                // just one binding
                vkCmdBindVertexBuffers(
                    drawCmdBuffers._array[i],
                    0u,
                    1u,
                    _vertices._vertexBuffer.ptr,
                    offsets
                )

                vkCmdBindIndexBuffer(
                    drawCmdBuffers._array[i],
                    _vertices._indexBuffer.value,
                    0u,
                    VK_INDEX_TYPE_UINT32
                )

                // Draw indexed buffer
                vkCmdDrawIndexed(
                    drawCmdBuffers._array[i],
                    _vertices.indicesCount.toUInt(), // vertices to draw
                    1u,                         // one instance
                    0u,                         // first index
                    0,                          // vertex offset
                    1u                          // first instance
                )

                vkCmdEndRenderPass(drawCmdBuffers._array[i])

                // Ending the render pass will add an implicit barrier transitioning the frame buffer color attachment to
                // VK_IMAGE_LAYOUT_PRESENT_SRC_KHR for presenting it to the windowing system

                if (!VK_CHECK(vkEndCommandBuffer(drawCmdBuffers._array[i])))
                    throw RuntimeException("Failed end command buffer")

            }


        }

    }

    override fun dispose() {
        vkFreeCommandBuffers(_device.device, _device.commandPool, drawCmdBuffers._size, drawCmdBuffers._array)
        super.dispose()
    }
}

