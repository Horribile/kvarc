/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc.graphics.vk

import kotlinx.cinterop.*
import kvarc.graphics.vk.utils.VK_CHECK
import kvarc.utils.DisposableContainer
import vulkan.*

@ExperimentalUnsignedTypes
internal class DescriptorPool(private val lDevice: LogicalDevice, val size: Int) : DisposableContainer() {

    private val _descriptorPool: VkDescriptorPoolVar = with(arena) { alloc() }

    val value by lazy {
        _descriptorPool.value
    }

    init {
        memScoped {

            //TODO change for size parameter
            val typeCounts = allocArray<VkDescriptorPoolSize>(size)
            typeCounts[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER
            typeCounts[0].descriptorCount = 1u

            // Create the global descriptor pool
            // All descriptors used in this example are allocated from this pool

            val descriptorPoolCreateInfo = alloc<VkDescriptorPoolCreateInfo>().apply {
                sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO
                pNext = null
                poolSizeCount = 1u
                pPoolSizes = typeCounts
                // just one descriptor
                maxSets = 1u
                flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT
            }

            if (!VK_CHECK(
                    vkCreateDescriptorPool(
                        lDevice.device,
                        descriptorPoolCreateInfo.ptr,
                        null,
                        _descriptorPool.ptr
                    )
                )
            )
                throw RuntimeException("failed create descriptor pool")

        }
    }

    override fun dispose() {

        vkDestroyDescriptorPool(lDevice.device, _descriptorPool.value, null)
        super.dispose()


    }
}