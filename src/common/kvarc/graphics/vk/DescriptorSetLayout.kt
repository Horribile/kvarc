/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc.graphics.vk

import kotlinx.cinterop.*
import kvarc.graphics.vk.utils.VK_CHECK
import kvarc.utils.DisposableContainer
import vulkan.*

// Connects the different shader stages to descriptors for binding uniform buffers, image samplers, etc.

@ExperimentalUnsignedTypes
internal class DescriptorSetLayout(private val lDevice: LogicalDevice) : DisposableContainer() {


    val value by lazy {
        _descriptorSetLayout.value
    }

    var _descriptorSetLayout: VkDescriptorSetLayoutVar = with(arena) { alloc() }
        private set

    init {

        memScoped {

            // Binding 0: Uniform buffer (Vertex shader)
            val layoutBinding = alloc<VkDescriptorSetLayoutBinding>().apply {
                binding = 0u
                descriptorCount = 1u
                descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER
                stageFlags = VK_SHADER_STAGE_VERTEX_BIT
                pImmutableSamplers = null
            }

            val descriptorLayout = alloc<VkDescriptorSetLayoutCreateInfo>().apply {
                sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO
                bindingCount = 1u
                pBindings = layoutBinding.ptr
                pNext = null
            }


            if (!VK_CHECK(
                    vkCreateDescriptorSetLayout(
                        lDevice.device,
                        descriptorLayout.ptr,
                        null,
                        _descriptorSetLayout.ptr
                    )
                )
            )
                throw RuntimeException("failed create descriptor set layout")

        }

    }

    override fun dispose() {

        vkDestroyDescriptorSetLayout(lDevice.device, _descriptorSetLayout.value, null)
        super.dispose()
    }


}