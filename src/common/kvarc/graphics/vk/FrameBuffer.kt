/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc.graphics.vk

import kotlinx.cinterop.*
import kvarc.graphics.vk.utils.VK_CHECK
import kvarc.utils.Disposable
import kvarc.utils.DisposableContainer
import vulkan.*

@ExperimentalUnsignedTypes
class AttachmentCreateInfo(var width: UInt, var height: UInt, var layerCount: UInt, var format: VkFormat, var usage: VkImageUsageFlags)

@ExperimentalUnsignedTypes
internal class FrameBuffer(
    private val container: DisposableContainer,
    private val vDevice: LogicalDevice,
    private val pDevice: PhysicalDevice,
    private val width: UInt,
    private val height: UInt
) : Disposable {

    val framebuffer: VkFramebufferVar = with(container.arena) { alloc() }
    val renderPass: VkRenderPassVar = with(container.arena) { alloc() }
    var sampler: VkSamplerVar = with(container.arena) { alloc() }

    private val attachments: ArrayList<FrameBufferAttachment> = ArrayList()

    /**
     * Add a new attachment described by createinfo to the framebuffer's attachment list
     *
     * @param createinfo Structure that specifices the framebuffer to be constructed
     *
     * @return Index of the new attachment
     */
    fun addAttachment(createinfo: AttachmentCreateInfo): UInt {

        val attachment: FrameBufferAttachment = FrameBufferAttachment(createinfo.format)

        var aspectMask: VkImageAspectFlags = 0u
        if ((createinfo.usage and VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT) > 0u)
            aspectMask = VK_IMAGE_ASPECT_COLOR_BIT
        if ((createinfo.usage and VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT) > 0u) {
            if (attachment.hasDepth)
                aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT
            if (attachment.hasStencil)
                aspectMask = aspectMask or VK_IMAGE_ASPECT_STENCIL_BIT
        }

        assert(aspectMask > 0u)

        attachment.subresourceRange.aspectMask = aspectMask
        attachment.subresourceRange.levelCount = 1u
        attachment.subresourceRange.layerCount = createinfo.layerCount

        memScoped {

            val image = alloc<VkImageCreateInfo>().apply {
                sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO
                imageType = VK_IMAGE_TYPE_2D
                format = createinfo.format
                extent.width = createinfo.width
                extent.height = createinfo.height
                extent.depth = 1u
                mipLevels = 1u
                arrayLayers = createinfo.layerCount
                samples = VK_SAMPLE_COUNT_1_BIT
                tiling = VK_IMAGE_TILING_OPTIMAL
                usage = createinfo.usage
            }

            val memAlloc = alloc<VkMemoryAllocateInfo>().apply {
                sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO
            }

            if (!VK_CHECK(vkCreateImage(vDevice.device, image.ptr, null, attachment.image.ptr))) {
                throw RuntimeException("Failed to create image");
            }

            val memReqs: VkMemoryRequirements = alloc()

            vkGetImageMemoryRequirements(vDevice.device, attachment.image.value, memReqs.ptr)

            memAlloc.allocationSize = memReqs.size
            memAlloc.memoryTypeIndex = pDevice.getMemoryType(memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)

            if (!VK_CHECK(vkAllocateMemory(vDevice.device, memAlloc.ptr, null, attachment.memory.ptr))) {
                throw RuntimeException("Could not allocate memory")
            }

            if (!VK_CHECK(vkBindImageMemory(vDevice.device, attachment.image.value, attachment.memory.value, 0u))) {
                throw RuntimeException("Could not bind memory")
            }


            val imageView: VkImageViewCreateInfo = alloc<VkImageViewCreateInfo>().apply {
                sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO
                viewType = if (createinfo.layerCount == 1u) VK_IMAGE_VIEW_TYPE_2D else VK_IMAGE_VIEW_TYPE_2D_ARRAY
                format = createinfo.format
                subresourceRange.aspectMask = attachment.subresourceRange.aspectMask
                subresourceRange.levelCount = attachment.subresourceRange.levelCount
                subresourceRange.layerCount = attachment.subresourceRange.layerCount
                subresourceRange.aspectMask = if (attachment.hasDepth) VK_IMAGE_ASPECT_DEPTH_BIT else aspectMask
                this.image = attachment.image.value
            }

            if (!VK_CHECK(vkCreateImageView(vDevice.device, imageView.ptr, null, attachment.view.ptr)))
                throw RuntimeException("Failed to create image view")

            attachment.description.samples = VK_SAMPLE_COUNT_1_BIT
            attachment.description.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR
            attachment.description.storeOp = if ((createinfo.usage and VK_IMAGE_USAGE_SAMPLED_BIT) > 0u) VK_ATTACHMENT_STORE_OP_STORE else VK_ATTACHMENT_STORE_OP_DONT_CARE
            attachment.description.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE
            attachment.description.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE
            attachment.description.format = createinfo.format
            attachment.description.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED

            // Final layout
            // If not, final layout depends on attachment type
            if (attachment.hasDepth || attachment.hasStencil)
                attachment.description.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL
            else
                attachment.description.finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL

        }

        attachments.add(attachment)

        return (attachments.size - 1).toUInt()
    }

    /**
     * Creates a default sampler for sampling from any of the framebuffer attachments
     * Applications are free to create their own samplers for different use cases
     *
     * @param magFilter Magnification filter for lookups
     * @param minFilter Minification filter for lookups
     * @param adressMode Adressing mode for the U,V and W coordinates
     *
     * @return VkResult for the sampler creation
     */

    fun createSampler(magFilter: VkFilter, minFilter: VkFilter, adressMode: VkSamplerAddressMode): VkResult {
        memScoped {

            val samplerInfo: VkSamplerCreateInfo = alloc<VkSamplerCreateInfo>().apply {
                sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO
                this.magFilter = magFilter
                this.minFilter = minFilter
                maxAnisotropy = 1.0f
                mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR
                addressModeU = adressMode
                addressModeV = adressMode
                addressModeW = adressMode
                mipLodBias = 0.0f
                minLod = 0.0f
                maxLod = 1.0f
                borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE

            }

            return vkCreateSampler(vDevice.device, samplerInfo.ptr, null, sampler.ptr)

        }
    }

    /**
     * Creates a default render pass setup with one sub pass
     *
     * @return VK_SUCCESS if all resources have been created successfully
     */
    fun createRenderPass(): VkResult {

        memScoped {

            val depthReference: VkAttachmentReference = alloc()

            var hasDepth = false
            var hasColor = false

            var attachmentIndex = 0u

            //val colorReferences = arrayOfNulls<VkAttachmentReference>(attachments.size)
            val refs = ArrayList<VkAttachmentReference>()


            attachments.forEach {
                if (it.isDepthStencil) {

                    assert(!hasDepth)
                    depthReference.attachment = attachmentIndex
                    depthReference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
                    hasDepth = true

                } else {
                    val ref = alloc<VkAttachmentReference>()
                    ref.attachment = attachmentIndex
                    ref.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
                    refs.add(ref)
                    hasColor = true

                }
                attachmentIndex++
            }

            val colorReferences = allocArray<VkAttachmentReference>(refs.size)
            for (i in 0 until refs.size) {
                colorReferences[i].layout = refs[i].layout
                colorReferences[i].attachment = refs[i].attachment
            }

            // Default render pass setup uses only one subpass
            val subpass = alloc<VkSubpassDescription>()
            subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS

            if (hasColor) {
                subpass.pColorAttachments = colorReferences
                subpass.colorAttachmentCount = refs.size.toUInt()
            }

            if (hasDepth) {
                subpass.pDepthStencilAttachment = depthReference.ptr
            }

            // Use subpass dependencies for attachment layout transitions
            val dependencies = allocArray<VkSubpassDependency>(2)

            dependencies[0].apply {
                srcSubpass = VK_SUBPASS_EXTERNAL
                dstSubpass = 0u
                srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT
                dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT
                srcAccessMask = VK_ACCESS_MEMORY_READ_BIT
                dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT or VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT
                dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT

            }
            dependencies[1].apply {
                srcSubpass = 0u
                dstSubpass = VK_SUBPASS_EXTERNAL
                srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT
                dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT
                srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT or VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT
                dstAccessMask = VK_ACCESS_MEMORY_READ_BIT
                dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT
            }


            var idx = 0
            val descriptions = allocArray<VkAttachmentDescription>(attachments.size) {
                val desc = attachments[idx++].description
                this.format = desc.format
                this.finalLayout = desc.finalLayout
                this.flags = desc.flags
                this.initialLayout = desc.initialLayout
                this.loadOp = desc.loadOp
                this.samples = desc.samples
                this.stencilLoadOp = desc.stencilLoadOp
                this.stencilStoreOp = desc.stencilStoreOp
                this.storeOp = desc.stencilStoreOp
            }


            val renderPassInfo = alloc<VkRenderPassCreateInfo>().apply {
                sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO
                pAttachments = descriptions
                attachmentCount = attachments.size.toUInt()
                subpassCount = 1u
                pSubpasses = subpass.ptr
                dependencyCount = 2u
                pDependencies = dependencies
            }

            if (!VK_CHECK(vkCreateRenderPass(vDevice.device, renderPassInfo.ptr, null, renderPass.ptr)))
                throw RuntimeException("Failed to create Renderpass")

            idx = 0
            val imgs = allocArray<VkImageViewVar>(attachments.size) {
                this.value = attachments[idx++].view.value
            }

            // Find. max number of layers across attachments
            var maxLayers = 0u
            attachments.forEach {
                if (it.subresourceRange.layerCount > maxLayers)
                    maxLayers = it.subresourceRange.layerCount
            }

            val framebufferInfo = alloc<VkFramebufferCreateInfo>().apply {
                sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO
                renderPass = this@FrameBuffer.renderPass.value
                pAttachments = imgs
                attachmentCount = attachments.size.toUInt()
                width = this@FrameBuffer.width
                height = this@FrameBuffer.height
                layers = maxLayers

            }

            if (!VK_CHECK(vkCreateFramebuffer(vDevice.device, framebufferInfo.ptr, null, framebuffer.ptr)))
                throw RuntimeException("Failed to create framebuffer")


        }

        return VK_SUCCESS

    }


    override fun dispose() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


}
