package kvarc.graphics.vk

import kotlinx.cinterop.*
import vulkan.VkExtensionProperties
import vulkan.VkLayerProperties
import vulkan.VkResult

@ExperimentalUnsignedTypes
val Instance.instanceExtensions : MutableList<String>
get() {

    val availableInstanceExtensions: MutableList<String> = ArrayList()

    memScoped {

        val extensionsCount = alloc<UIntVar>()
        extensionsCount.value = 0u
        var result: VkResult

        // Enumerate instance extsensions and check if they're available
        do {

            result = vulkan.vkEnumerateInstanceExtensionProperties(null, extensionsCount.ptr, null)

            if (!kvarc.graphics.vk.utils.VK_CHECK(result)) throw kotlin.RuntimeException("Could not enumerate instance extensions.")

            if (extensionsCount.value == 0u) break

            val buffer = allocArray<VkExtensionProperties>(extensionsCount.value.toInt())
            result = vulkan.vkEnumerateInstanceExtensionProperties(null, extensionsCount.ptr, buffer)

            for (i in 0 until extensionsCount.value.toInt()) {
                val ext = buffer[i].extensionName.toKString()
                if (!availableInstanceExtensions.contains(ext))
                    availableInstanceExtensions.add(ext)
            }


        } while (result == vulkan.VK_INCOMPLETE)

    }

    return availableInstanceExtensions
}

@ExperimentalUnsignedTypes
val Instance.debugLayers : MutableList<String>
        get() {

            val availableLayers = mutableListOf<String>()

            memScoped {

                // Layers optimal order: <a href='https://vulkan.lunarg.com/doc/view/1.0.13.0/windows/layers.html'/>
                val layers = kotlin.arrayOf(
                    "VK_LAYER_GOOGLE_threading",
                    "VK_LAYER_LUNARG_parameter_validation",
                    "VK_LAYER_LUNARG_object_tracker",
                    "VK_LAYER_LUNARG_core_validation",
                    "VK_LAYER_GOOGLE_unique_objects",
                    "VK_LAYER_LUNARG_standard_validation"
                )

                val layersCount = alloc<UIntVar>()

                var result: VkResult

                run failure@{

                    do {

                        // Enumerate available layers
                        result = vulkan.vkEnumerateInstanceLayerProperties(layersCount.ptr, null)
                        if (!kvarc.graphics.vk.utils.VK_CHECK(result)) {
                            kvarc.utils.logError("Failed to enumerate debug layers")
                            availableLayers.clear()
                            return@failure // failed to get layers break the loop

                        } else {

                            val buffer = allocArray<VkLayerProperties>(layersCount.value.toInt())

                            result = vulkan.vkEnumerateInstanceLayerProperties(layersCount.ptr, buffer)
                            if (!kvarc.graphics.vk.utils.VK_CHECK(result)) {
                                kvarc.utils.logError("Filed to enumerate Debug Layers to buffer")
                                availableLayers.clear()
                                return@failure // failed to get layers break the loop

                            }

                            for (i in 0 until layersCount.value.toInt()) {

                                val layer = buffer[i].layerName.toKString()
                                if (!availableLayers.contains(layer) && layers.contains(layer)) {
                                    availableLayers.add(layer)
                                }

                            }

                        }

                    } while (result == vulkan.VK_INCOMPLETE)

                }

                // Setting debug layers it they're available
                if (availableLayers.size > 0) {

                    if (availableLayers.contains("VK_LAYER_LUNARG_standard_validation"))
                        availableLayers.removeAll {
                            it != "VK_LAYER_LUNARG_standard_validation"
                        }
                    else
                    // sort available layers in accordance with recommended order
                        availableLayers.sortBy {
                            layers.indexOf(it)
                        }

                }

            }

            return availableLayers
        }