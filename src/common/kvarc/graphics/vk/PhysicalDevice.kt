/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc.graphics.vk

import kotlinx.cinterop.*
import kvarc.graphics.vk.utils.VK_CHECK
import kvarc.utils.DisposableContainer
import vulkan.*

@ExperimentalUnsignedTypes
internal class PhysicalDevice(val instance: VkInstance, val surface: VkSurfaceKHR) : DisposableContainer() {

    private var _device: VkPhysicalDeviceVar = arena.alloc()
    private var _memoryProperties: VkPhysicalDeviceMemoryProperties = arena.alloc()
    private val _deviceProperties = arena.alloc<VkPhysicalDeviceProperties>()
    private val _deviceFeatures = arena.alloc<VkPhysicalDeviceFeatures>()
    private val _surfaceCapabilities: VkSurfaceCapabilitiesKHR = arena.alloc()
    private lateinit var _queueProperties: ArrayList<VkQueueFamilyProperties>

    val device by lazy {
        _device.value
    }

    val memoryProperties: VkPhysicalDeviceMemoryProperties
        get() {
            vkGetPhysicalDeviceMemoryProperties(device, _memoryProperties.ptr)
            return _memoryProperties
        }

    init {
        try {

            memScoped {

                var result = VK_INCOMPLETE
                val gpuCount = alloc<UIntVar>()

                var buffer: CArrayPointer<VkPhysicalDeviceVar>? = null

                while (result == VK_INCOMPLETE) {

                    result = vkEnumeratePhysicalDevices(instance, gpuCount.ptr, null)
                    if (!VK_CHECK(result)){
                        throw RuntimeException("Could not enumerate GPUs.")
                    }

                    buffer?.let {
                        nativeHeap.free(it)
                        buffer = null
                    }

                    buffer = nativeHeap.allocArray(gpuCount.value.toInt())

                    result = vkEnumeratePhysicalDevices(instance, gpuCount.ptr, buffer)
                    if (result != VK_INCOMPLETE && !VK_CHECK(result))
                        throw RuntimeException("Could not enumerate GPUs.")

                }

                _device.value = buffer!![0]

                for (i in 0 until gpuCount.value.toInt()) {

                    val props = alloc<VkPhysicalDeviceProperties>()
                    vkGetPhysicalDeviceProperties(buffer!![i], props.ptr)

                    if (props.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) {
                        _device.value = buffer!![i]
                        break
                    }

                }

                buffer?.let {
                    nativeHeap.free(it)
                    buffer = null
                }

            }

        }
        catch (e:Exception) {
            dispose()
            throw e
        }
    }

    val deviceProperties: VkPhysicalDeviceProperties by lazy {

        assert(device != null)

        vkGetPhysicalDeviceProperties(device, _deviceProperties.ptr)
        _deviceProperties
    }

    val deviceFeatures by lazy {

        assert(device != null)

        vkGetPhysicalDeviceFeatures(device, _deviceFeatures.ptr)
        _deviceFeatures

    }

    val surfaceCapabilities: VkSurfaceCapabilitiesKHR by lazy {

        assert(device != null)

        val result = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, _surfaceCapabilities.ptr)
        if(!VK_CHECK(result)){
            throw RuntimeException("Failed to get surface capabilities")
        }
        _surfaceCapabilities
    }

    val queueProperties by lazy {

        assert(device != null)

        if (!::_queueProperties.isInitialized) {

            _queueProperties = ArrayList()

            memScoped {

                val queueFamilyCount: UIntVar = alloc()
                vkGetPhysicalDeviceQueueFamilyProperties(device, queueFamilyCount.ptr, null)
                assert(queueFamilyCount.value > 0u)

                val buffer = arena.allocArray<VkQueueFamilyProperties>(queueFamilyCount.value.toInt())
                vkGetPhysicalDeviceQueueFamilyProperties(device, queueFamilyCount.ptr, buffer)

                for (i in 0 until queueFamilyCount.value.toInt()) {
                    _queueProperties.add(buffer[i])
                }

            }

        }

        _queueProperties

    }

    val presentableQueue by lazy {

        val props = queueProperties.filter {
            (it.queueFlags and VK_QUEUE_GRAPHICS_BIT) > 0u
        }

        memScoped {

            props.mapIndexed { index, it ->
                Pair(index, it)
            }.indexOfFirst {
                val p = alloc<VkBool32Var>()
                vkGetPhysicalDeviceSurfaceSupportKHR(device, it.first.toUInt(), surface, p.ptr)
                if (p.value == 1u) true else false
            }

        }

    }

    val extensionProperties by lazy {

        val props: ArrayList<String> = ArrayList()
        memScoped {

            val count = alloc<UIntVar>()
            vkEnumerateDeviceExtensionProperties(device, null, count.ptr, null)
            val arr = allocArray<VkExtensionProperties>(count.value.toInt())
            vkEnumerateDeviceExtensionProperties(device, null, count.ptr, arr)
            for (i in 0 until count.value.toInt())
                props.add(arr[i].extensionName.toKString())

        }
        props

    }

    // Graphics queue already contains transfer
    fun getQueueIndex(queueFlags: VkQueueFlagBits): UInt {

        val props = queueProperties

        when {
            (queueFlags and VK_QUEUE_GRAPHICS_BIT) > 0u -> {

                val idx = props.indexOfFirst {
                    (it.queueFlags and VK_QUEUE_GRAPHICS_BIT) > 0u
                }

                if (idx != -1) return idx.toUInt() else throw RuntimeException("Queue does not exist")
            }
            (queueFlags and VK_QUEUE_COMPUTE_BIT) > 0u -> {
                val idx = props.indexOfFirst {
                    (it.queueFlags and VK_QUEUE_COMPUTE_BIT) > 0u
                }
                if (idx != -1) return idx.toUInt() else throw RuntimeException("Queue does not exist")
            }
            (queueFlags and VK_QUEUE_TRANSFER_BIT) > 0u -> {
                val idx = props.indexOfFirst {
                    (it.queueFlags and VK_QUEUE_COMPUTE_BIT) > 0u
                }
                if (idx != -1) return idx.toUInt() else throw RuntimeException("Queue does not exist")
            }
            else -> {
                throw RuntimeException("Invalid queue requested")
            }
        }

    }


}