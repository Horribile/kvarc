/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc.graphics.vk

import kotlinx.cinterop.*
import kvarc.utils.DisposableContainer
import vulkan.*

@ExperimentalUnsignedTypes
class FrameBufferAttachment(var format: VkFormat) : DisposableContainer() {

    var image: VkImageVar
    var memory: VkDeviceMemoryVar
    var view: VkImageViewVar
    var subresourceRange: VkImageSubresourceRange
    var description: VkAttachmentDescription

    init {
        with(arena) {
            image = alloc()
            memory = alloc()
            view = alloc()
            subresourceRange = alloc()
            description = alloc()


        }
    }

    val hasDepth by lazy {

        val formats = arrayOf(
            VK_FORMAT_D16_UNORM,
            VK_FORMAT_X8_D24_UNORM_PACK32,
            VK_FORMAT_D32_SFLOAT,
            VK_FORMAT_D16_UNORM_S8_UINT,
            VK_FORMAT_D24_UNORM_S8_UINT,
            VK_FORMAT_D32_SFLOAT_S8_UINT
        )

        formats.firstOrNull() { it == format } != null
    }

    val hasStencil by lazy {

        val formats = arrayOf(
            VK_FORMAT_S8_UINT,
            VK_FORMAT_D16_UNORM_S8_UINT,
            VK_FORMAT_D24_UNORM_S8_UINT,
            VK_FORMAT_D32_SFLOAT_S8_UINT
        )

        formats.firstOrNull() { it == format } != null

    }

    val isDepthStencil by lazy {
        hasDepth && hasStencil
    }

}

