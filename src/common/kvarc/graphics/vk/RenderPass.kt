/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc.graphics.vk

import kvarc.utils.DisposableContainer
import kotlinx.cinterop.*
import kvarc.graphics.vk.utils.VK_CHECK
import vulkan.*

@ExperimentalUnsignedTypes
internal class RenderPass(
    private val lDevice: LogicalDevice,
    private val swapchain: SwapChain,
    private val pDevice: PhysicalDevice
) : DisposableContainer() {

    var renderPass: VkRenderPassVar = with(arena) { alloc() }

    val value by lazy {
        renderPass.value
    }

    init {

        memScoped {

            val attachments = allocArray<VkAttachmentDescription>(2).apply {

                this[0].format = swapchain.displayFormat
                this[0].samples = VK_SAMPLE_COUNT_1_BIT
                this[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR
                this[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE
                this[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE
                this[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE
                this[0].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED
                this[0].finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR

                this[1].format = pDevice.depthFormat
                this[1].samples = VK_SAMPLE_COUNT_1_BIT
                this[1].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR
                this[1].storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE
                this[1].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE
                this[1].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE
                this[1].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED
                this[1].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL


            }

            val colorReference = alloc<VkAttachmentReference>()
            colorReference.attachment = 0u
            colorReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL

            val depthReference = alloc<VkAttachmentReference>()
            depthReference.attachment = 1u
            depthReference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL

            // Setup a single subpass reference
            val subpassDescription = alloc<VkSubpassDescription>()
            subpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS
            subpassDescription.flags = 0u
            subpassDescription.inputAttachmentCount = 0u
            subpassDescription.pInputAttachments = null
            subpassDescription.colorAttachmentCount = 1u
            subpassDescription.pColorAttachments = colorReference.ptr
            subpassDescription.pResolveAttachments = null
            subpassDescription.pDepthStencilAttachment = null
            subpassDescription.preserveAttachmentCount = 0u
            subpassDescription.pPreserveAttachments = null
            subpassDescription.pDepthStencilAttachment = depthReference.ptr

            // subpass dependencies
            val dependencies = allocArray<VkSubpassDependency>(2).apply {

                this[0].srcSubpass = VK_SUBPASS_EXTERNAL
                this[0].dstSubpass = 0u
                this[0].srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT
                this[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT
                this[0].srcAccessMask = VK_ACCESS_MEMORY_READ_BIT
                this[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT or VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT
                this[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT

                this[1].srcSubpass = 0u
                this[1].dstSubpass = VK_SUBPASS_EXTERNAL
                this[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT
                this[1].dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT
                this[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT or VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT
                this[1].dstAccessMask = VK_ACCESS_MEMORY_READ_BIT
                this[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT
            }

            val renderPassCreateInfo = alloc<VkRenderPassCreateInfo>()
            renderPassCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO
            renderPassCreateInfo.pNext = null
            renderPassCreateInfo.attachmentCount = 2u
            renderPassCreateInfo.pAttachments = attachments
            renderPassCreateInfo.subpassCount = 1u
            renderPassCreateInfo.pSubpasses = subpassDescription.ptr
            renderPassCreateInfo.dependencyCount = 2u
            renderPassCreateInfo.pDependencies = dependencies

            if (!VK_CHECK(vkCreateRenderPass(lDevice.device, renderPassCreateInfo.ptr, null, renderPass.ptr))) {
                throw RuntimeException("Failed to initilaize render pass.")
            }


        }
    }

    override fun dispose() {

        vkDestroyRenderPass(lDevice.device, renderPass.value, null)
        super.dispose()
    }


}