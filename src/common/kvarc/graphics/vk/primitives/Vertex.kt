/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc.graphics.vk.primitives

import kotlinx.cinterop.*
import kvarc.graphics.math.Vec
import kvarc.graphics.math.Vec3
import vulkan.VK_FORMAT_R32G32B32_SFLOAT
import vulkan.VK_VERTEX_INPUT_RATE_VERTEX
import vulkan.VkVertexInputAttributeDescription
import vulkan.VkVertexInputBindingDescription

@ExperimentalUnsignedTypes
class Vertex(position: Vec3, color: Vec3) {
    var buffer: FloatArray = FloatArray(6) { 0f }
        private set

    var position: Vec3
        get() = Vec.fromBuffer(*buffer.copyOfRange(0, 3)) as Vec3
        set(value) {
            buffer[0] = value.x
            buffer[1] = value.y
            buffer[2] = value.z
        }

    var color: Vec3
        get() = Vec.fromBuffer(*buffer.copyOfRange(3, 6)) as Vec3
        set(value) {
            buffer[3] = value.x
            buffer[4] = value.y
            buffer[5] = value.z
        }

    init {
        this.position = position
        this.color = color
    }


    companion object {

        // binding point 0
        fun bindingDescription(scope: MemScope) = scope.alloc<VkVertexInputBindingDescription>().apply {
            binding = 0u
            stride = Vertex.SIZE.toUInt()
            inputRate = VK_VERTEX_INPUT_RATE_VERTEX
        }

        fun inputAtributes(scope: MemScope) = scope.allocArray<VkVertexInputAttributeDescription>(2).apply {

            // Inpute attribute bindings describe shader attribute locations and memory layouts

            // These match the following shader layout (see triangle.vert):
            //	layout (location = 0) in vec3 inPos;
            //	layout (location = 1) in vec3 inColor;

            // Attribute location 0: Position
            this[0].binding = 0u
            this[0].location = 0u
            // Position attribute is three 32 bit signed (SFLOAT) floats (R32 G32 B32)
            this[0].format = VK_FORMAT_R32G32B32_SFLOAT
            this[0].offset = 0u

            // Attribute location 1: Color
            this[1].binding = 0u
            this[1].location = 1u
            // Color attribute is three 32 bit signed (SFLOAT) floats (R32 G32 B32)
            this[1].format = VK_FORMAT_R32G32B32_SFLOAT
            this[1].offset = (Vertex.SIZE / 2).toUInt()

        }

        val SIZE = 6 * sizeOf<FloatVar>()
        val BUFFER_SIZE = 6
    }

}
