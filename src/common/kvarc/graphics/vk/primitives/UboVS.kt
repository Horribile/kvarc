/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc.graphics.vk.primitives

import kotlinx.cinterop.FloatVar
import kotlinx.cinterop.sizeOf
import kvarc.graphics.math.Mat4

@ExperimentalUnsignedTypes
abstract class Ubo {
    abstract val buffer: FloatArray
}

// For simplicity we use the same uniform block layout as in the shader:
//
//	layout(set = 0, binding = 0) uniform UBO
//	{
//		mat4 projectionMatrix;
//		mat4 modelMatrix;
//		mat4 viewMatrix;
//	} ubo;
//
// This way we can just memcopy the ubo data to the ubo
// Note: You should use data types that align with the GPU in order to avoid manual padding (vec4, mat4)
@ExperimentalUnsignedTypes
internal class UboVS(projectionMatrix: Mat4, modelMatrix: Mat4, viewMatrix: Mat4) : Ubo() {
    var projectionMatrix: Mat4
        get() = Mat4.from(*buffer.copyOfRange(0, 16))
        set(value) {
            value.toBuffer().copyInto(buffer, 0, 0, 16)
        }

    var modelMatrix: Mat4
        get() = Mat4.from(*buffer.copyOfRange(16, 32))
        set(value) {
            value.toBuffer().copyInto(buffer, 16, 0, 16)
        }

    var viewMatrix: Mat4
        get() = Mat4.from(*buffer.copyOfRange(32, 48))
        set(value) {
            value.toBuffer().copyInto(buffer, 32, 0, 16)
        }


    override val buffer: FloatArray = FloatArray(16 * 3) { 0f }

    init {
        this.projectionMatrix = projectionMatrix
        this.modelMatrix = modelMatrix
        this.viewMatrix = viewMatrix
    }

    companion object {
        val SIZE: Long = 16 * 3 * sizeOf<FloatVar>()
    }


}