/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc.graphics.vk.primitives

import kotlinx.cinterop.*
import kvarc.graphics.math.Mat4
import kvarc.graphics.math.Vec3
import kvarc.graphics.math.radians
import kvarc.graphics.vk.LogicalDevice
import kvarc.graphics.vk.PhysicalDevice
import kvarc.graphics.vk.SwapChain
import kvarc.graphics.vk.getMemoryType
import kvarc.graphics.vk.utils.VK_CHECK
import kvarc.utils.DisposableContainer
import vulkan.*

@ExperimentalUnsignedTypes
internal class UniformBuffers(
    private val pDevice: PhysicalDevice,
    private val lDevice: LogicalDevice,
    private val swapchain: SwapChain

) : DisposableContainer() {

    val uniformBufferVS: UniformBufferVars = UniformBufferVars()
    val uboVS: UboVS = UboVS(Mat4.ZERO, Mat4.ZERO, Mat4.ZERO)

    init {

        memScoped {

            val memReqs = alloc<VkMemoryRequirements>()

            uniformBufferVS.buffer = with(arena) { alloc() }
            uniformBufferVS.memory = with(arena) { alloc() }

            val bufferCreateInfo = alloc<VkBufferCreateInfo>().apply {
                sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO
                usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT
                size = UboVS.SIZE.toULong()
            }

            if (!VK_CHECK(vkCreateBuffer(lDevice.device, bufferCreateInfo.ptr, null, uniformBufferVS.buffer!!.ptr)))
                throw RuntimeException("failed to create buffer")

            val memoryAllocateInfo = alloc<VkMemoryAllocateInfo>().apply {
                sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO
                pNext = null
                allocationSize = 0u
                memoryTypeIndex = 0u
            }

            vkGetBufferMemoryRequirements(lDevice.device, uniformBufferVS.buffer!!.value, memReqs.ptr)
            memoryAllocateInfo.allocationSize = memReqs.size

            memoryAllocateInfo.memoryTypeIndex = pDevice.getMemoryType(
                memReqs.memoryTypeBits,
                VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT or VK_MEMORY_PROPERTY_HOST_COHERENT_BIT
            )

            if (!VK_CHECK(vkAllocateMemory(lDevice.device, memoryAllocateInfo.ptr, null, uniformBufferVS.memory!!.ptr)))
                throw RuntimeException("failed allocate memory")


            if (!VK_CHECK(
                    vkBindBufferMemory(
                        lDevice.device,
                        uniformBufferVS.buffer!!.value,
                        uniformBufferVS.memory!!.value,
                        0u
                    )
                )
            )
                throw  RuntimeException("faied bind memory")


            // Store information in the uniform's descriptor that is used by the descriptor set
            uniformBufferVS.descriptor = with(arena) { alloc() }
            uniformBufferVS.descriptor!!.offset = 0u
            uniformBufferVS.descriptor!!.buffer = uniformBufferVS.buffer!!.value
            uniformBufferVS.descriptor!!.range = UboVS.SIZE.toULong()

        }

        uboVS.modelMatrix = Mat4.identity

        update()

    }


    fun update() {

        uboVS.projectionMatrix = Mat4.perspective(
            radians(60f),
            swapchain.width.toInt().toFloat() / swapchain.height.toInt().toFloat(),
            0.1f, 256f
        )

        uboVS.viewMatrix = Mat4.translate(Mat4.identity, Vec3(0f, 0f, -5f /*zoom*/))

        uboVS.modelMatrix = Mat4.rotate(uboVS.modelMatrix, radians(1f), Vec3(z = 1f))

        memScoped {

            val data = alloc<COpaquePointerVar>()

            if (!VK_CHECK(
                    vkMapMemory(
                        lDevice.device!!,
                        uniformBufferVS.memory!!.value,
                        0u,
                        UboVS.SIZE.toULong(),
                        0u,
                        data.ptr
                    )
                )
            )
                throw RuntimeException("failed bind memory")

            uboVS.buffer.usePinned { buffer ->
                platform.posix.memcpy(data.value, buffer.addressOf(0), UboVS.SIZE.toULong())
            }

            vkUnmapMemory(lDevice.device, uniformBufferVS.memory!!.value)

        }

    }

    override fun dispose() {

        vkDestroyBuffer(lDevice.device, uniformBufferVS.buffer!!.value, null) //It should be prior
        vkFreeMemory(lDevice.device, uniformBufferVS.memory!!.value, null)

        super.dispose()
    }


}