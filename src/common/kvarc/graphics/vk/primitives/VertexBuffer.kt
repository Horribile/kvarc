/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc.graphics.vk.primitives

import kotlinx.cinterop.*
import kotlinx.cinterop.nativeHeap.alloc
import kvarc.graphics.math.Vec3
import kvarc.graphics.vk.LogicalDevice
import kvarc.graphics.vk.PhysicalDevice
import kvarc.graphics.vk.getMemoryType
import kvarc.graphics.vk.utils.VK_CHECK
import kvarc.utils.DisposableContainer
import vulkan.*

// A note on memory management in Vulkan in general:
//	This is a very complex topic and while it's fine for an example application to to small individual memory allocations that is not
//	what should be done a real-world application, where you should allocate large chunkgs of memory at once isntead.

// Static data like vertex and index buffer should be stored on the _device memory
// for optimal (and fastest) access by the GPU
//
// To achieve this we use so-called "staging buffers" :
// - Create a buffer that's visible to the host (and can be mapped)
// - Copy the data to this buffer
// - Create another buffer that's local on the _device (VRAM) with the same size
// - Copy the data from the host to the _device using a command buffer
// - Delete the host visible (staging) buffer
// - Use the _device local buffers for rendering

// TODO add operators, check dispose
private class StagingBuffer : DisposableContainer() {

    var memory: VkDeviceMemoryVar
        private set
    var buffer: VkBufferVar
        private set

    init {
        with(arena) {
            buffer = alloc()
            memory = alloc()
        }
    }
}

private class StagingBuffers {
    val vertices = StagingBuffer()
    val indices = StagingBuffer()
}


@ExperimentalUnsignedTypes
internal class VertexBuffer(
    private val pDevice: PhysicalDevice,
    private val lDevice: LogicalDevice,
    vertices: Array<Vertex>,
    indices: Array<UInt>
) : DisposableContainer() {

    var _vertexMemory: VkDeviceMemoryVar = with(arena) { alloc() }
        private set
    var _vertexBuffer: VkBufferVar = with(arena) { alloc() }
        private set

    var _indexMemory: VkDeviceMemoryVar = with(arena) { alloc() }
        private set
    var _indexBuffer: VkBufferVar = with(arena) { alloc() }
        private set

    private var vertexBuffer: FloatArray =
        FloatArray(vertices.size * Vertex.BUFFER_SIZE)

    private var indexBuffer: UIntArray = UIntArray(indices.size)

    val vertexBufferSize by lazy {
        vertexBuffer.size * sizeOf<FloatVar>()
    }

    val indexBufferSize by lazy {
        indexBuffer.size * sizeOf<UIntVar>()
    }

    val indicesCount by lazy {
        indexBuffer.size
    }

    init {

        var voffset = 0

        vertices.forEach { v ->
            v.buffer.copyInto(vertexBuffer, voffset)
            voffset += Vertex.BUFFER_SIZE
        }

        indices.forEachIndexed { index, uInt ->
            indexBuffer[index] = uInt
        }

        memScoped {

            val memoryAllocateInfo = alloc<VkMemoryAllocateInfo>().apply {
                sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO
            }

            val memReqs: VkMemoryRequirements = alloc()

            // Vertex buffer
            val vertexBufferInfo = alloc<VkBufferCreateInfo>().apply {
                sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO
                size = vertexBufferSize.toULong()
                usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT //copy source
            }

            val stagingBuffers = StagingBuffers()

            if (!VK_CHECK(
                    vkCreateBuffer(
                        lDevice.device,
                        vertexBufferInfo.ptr,
                        null,
                        stagingBuffers.vertices.buffer.ptr
                    )
                )
            )
                throw RuntimeException("Failed to create buffer")

            vkGetBufferMemoryRequirements(lDevice.device, stagingBuffers.vertices.buffer.value, memReqs.ptr)

            memoryAllocateInfo.allocationSize = memReqs.size

            // host visible memory and coherent
            memoryAllocateInfo.memoryTypeIndex = pDevice.getMemoryType(
                memReqs.memoryTypeBits,
                VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT or VK_MEMORY_PROPERTY_HOST_COHERENT_BIT
            )

            val mapped = alloc<COpaquePointerVar>()

            if (!VK_CHECK(
                    vkAllocateMemory(
                        lDevice.device,
                        memoryAllocateInfo.ptr,
                        null,
                        stagingBuffers.vertices.memory.ptr
                    )
                )
            )
                throw RuntimeException("Faild allocate memory")

            if (!VK_CHECK(
                    vkMapMemory(
                        lDevice.device,
                        stagingBuffers.vertices.memory.value,
                        0u,
                        memoryAllocateInfo.allocationSize,
                        0u,
                        mapped.ptr
                    )
                )
            )
                throw RuntimeException("Faild map memory")

            vertexBuffer.usePinned { buffer ->
                platform.posix.memcpy(mapped.value, buffer.addressOf(0), vertexBufferSize.toULong())
            }

            vkUnmapMemory(lDevice.device, stagingBuffers.vertices.memory.value)

            if (!VK_CHECK(
                    vkBindBufferMemory(
                        lDevice.device,
                        stagingBuffers.vertices.buffer.value,
                        stagingBuffers.vertices.memory.value,
                        0u
                    )
                )
            )
                throw RuntimeException("failed bind memory")

            // Create a _device local buffer to accept data
            vertexBufferInfo.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT or VK_BUFFER_USAGE_TRANSFER_DST_BIT

            if (!VK_CHECK(vkCreateBuffer(lDevice.device, vertexBufferInfo.ptr, null, _vertexBuffer.ptr)))
                throw RuntimeException("Failed to create buffer")
            vkGetBufferMemoryRequirements(lDevice.device, _vertexBuffer.value, memReqs.ptr)

            memoryAllocateInfo.allocationSize = memReqs.size
            memoryAllocateInfo.memoryTypeIndex = pDevice.getMemoryType(
                memReqs.memoryTypeBits,
                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
            )

            if (!VK_CHECK(vkAllocateMemory(lDevice.device, memoryAllocateInfo.ptr, null, _vertexMemory.ptr)))
                throw RuntimeException("Faild allocate memory")

            if (!VK_CHECK(vkBindBufferMemory(lDevice.device, _vertexBuffer.value, _vertexMemory.value, 0u)))
                throw RuntimeException("failed bind memory")

            // Index buffer

            val indexbufferInfo = alloc<VkBufferCreateInfo>().apply {
                sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO
                size = indexBufferSize.toULong()
                usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT
            }

            // Copy index data to a buffer visible to the host (staging buffer)
            val imapped = alloc<COpaquePointerVar>()

            if (!VK_CHECK(vkCreateBuffer(lDevice.device, indexbufferInfo.ptr, null, stagingBuffers.indices.buffer.ptr)))
                throw RuntimeException("Failed to create buffer")

            vkGetBufferMemoryRequirements(lDevice.device, stagingBuffers.indices.buffer.value, memReqs.ptr)
            memoryAllocateInfo.allocationSize = memReqs.size
            memoryAllocateInfo.memoryTypeIndex = pDevice.getMemoryType(
                memReqs.memoryTypeBits,
                VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT or VK_MEMORY_PROPERTY_HOST_COHERENT_BIT
            )

            if (!VK_CHECK(
                    vkAllocateMemory(
                        lDevice.device,
                        memoryAllocateInfo.ptr,
                        null,
                        stagingBuffers.indices.memory.ptr
                    )
                )
            )
                throw RuntimeException("Failed allocate memory")

            if (!VK_CHECK(
                    vkMapMemory(
                        lDevice.device,
                        stagingBuffers.indices.memory.value,
                        0u,
                        memoryAllocateInfo.allocationSize,
                        0u,
                        imapped.ptr
                    )
                )
            )
                throw RuntimeException("Failed map memory")

            indexBuffer.usePinned { buffer ->
                platform.posix.memcpy(imapped.value, buffer.addressOf(0), indexBufferSize.toULong())
            }

            vkUnmapMemory(lDevice.device, stagingBuffers.indices.memory.value)

            if (!VK_CHECK(
                    vkBindBufferMemory(
                        lDevice.device,
                        stagingBuffers.indices.buffer.value,
                        stagingBuffers.indices.memory.value,
                        0u
                    )
                )
            )
                throw RuntimeException("failed bind memory")

            // Create destination buffer with _device only visibility
            indexbufferInfo.usage = VK_BUFFER_USAGE_INDEX_BUFFER_BIT or VK_BUFFER_USAGE_TRANSFER_DST_BIT

            if (!VK_CHECK(vkCreateBuffer(lDevice.device, indexbufferInfo.ptr, null, _indexBuffer.ptr)))
                throw RuntimeException("Failed to create buffer")

            vkGetBufferMemoryRequirements(lDevice.device, _indexBuffer.value, memReqs.ptr)

            memoryAllocateInfo.allocationSize = memReqs.size
            memoryAllocateInfo.memoryTypeIndex = pDevice.getMemoryType(
                memReqs.memoryTypeBits,
                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
            )

            if (!VK_CHECK(vkAllocateMemory(lDevice.device, memoryAllocateInfo.ptr, null, _indexMemory.ptr)))
                throw RuntimeException("Faild allocate memory")

            if (!VK_CHECK(vkBindBufferMemory(lDevice.device, _indexBuffer.value, _indexMemory.value, 0u)))
                throw RuntimeException("failed bind memory")

            //TODO rewrite with this use VulkanBuffer
            //lDevice.copyBuffer(stagingBuffers.vertices.buffer.value)

            val copyCmd = lDevice.createCommandBuffers(VK_COMMAND_BUFFER_LEVEL_PRIMARY, 1u, true)

            // Put buffer region copies into command buffer
            val copyRegion = alloc<VkBufferCopy>()

            copyRegion.size = vertexBufferSize.toULong()
            vkCmdCopyBuffer(
                copyCmd._array[0],
                stagingBuffers.vertices.buffer.value,
                _vertexBuffer.value,
                1u,
                copyRegion.ptr
            )

            copyRegion.size = indexBufferSize.toULong()
            vkCmdCopyBuffer(
                copyCmd._array[0],
                stagingBuffers.indices.buffer.value,
                _indexBuffer.value,
                1u,
                copyRegion.ptr
            )

            val cc = alloc<VkCommandBufferVar>()
            cc.value = copyCmd._array[0]
            lDevice.flushCommandBuffer(cc, false)

            vkDestroyBuffer(lDevice.device, stagingBuffers.vertices.buffer.value, null)
            vkFreeMemory(lDevice.device, stagingBuffers.vertices.memory.value, null)

            vkDestroyBuffer(lDevice.device, stagingBuffers.indices.buffer.value, null)
            vkFreeMemory(lDevice.device, stagingBuffers.indices.memory.value, null)

        }

    }

    companion object {

        // Vertex to index
        fun Triangle(pDevice: PhysicalDevice, lDevice: LogicalDevice): VertexBuffer = VertexBuffer(
            pDevice,
            lDevice,
            arrayOf(
                Vertex(Vec3(0.866f, 0.5f, 0.0f), Vec3(1.0f, 0.0f, 0.0f)),
                Vertex(Vec3(-0.866f, 0.5f, 0.0f), Vec3(0.0f, 1.0f, 0.0f)),
                Vertex(Vec3(0.0f, -1.0f, 0.0f), Vec3(0.0f, 0.0f, 1.0f))
            ),
            arrayOf(0u, 2u, 1u)
        )

    }


}