/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc.graphics.vk

import kotlinx.cinterop.*
import kvarc.graphics.vk.utils.VK_CHECK
import kvarc.utils.DisposableContainer
import vulkan.*

//TODO use FrameBuffer class
@ExperimentalUnsignedTypes
internal class FrameBuffers(
    private val _device: LogicalDevice,
    private val _renderpass: RenderPass,
    private val _swapchain: SwapChain,
    private val _deptstensil: DepthStencil

) : DisposableContainer() {

    var frameBuffers = ArrayList<VkFramebufferVar>().apply {
        for (i in 0 until _swapchain.imageBuffers.size) {
            this.add(with(arena) { alloc<VkFramebufferVar>() })
        }
    }
        private set

    init {

        ArrayList<Int>()
        for (i in 0 until frameBuffers.size) {

            memScoped {


                val attachments = allocArray<VkImageViewVar>(2)
                attachments[0] = _swapchain.imageBuffers[i].second.value
                attachments[1] = _deptstensil.imageView!!.value

                val frameBufferCreateInfo = alloc<VkFramebufferCreateInfo>().apply {
                    sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO
                    renderPass = _renderpass.renderPass.value // the same for all
                    attachmentCount = 2u
                    pAttachments = attachments
                    width = _swapchain.width
                    height = _swapchain.height
                    layers = 1u
                }

                if (!VK_CHECK(
                        vkCreateFramebuffer(
                            _device.device,
                            frameBufferCreateInfo.ptr,
                            null,
                            frameBuffers[i].ptr
                        )
                    )
                )
                    throw RuntimeException("Failed create framebuffers")


            }


        }

    }

    override fun dispose() {
        frameBuffers.forEach {
            vkDestroyFramebuffer(_device.device, it.value, null)
        }
        super.dispose()
    }

}

