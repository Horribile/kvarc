/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc.graphics.vk

import kotlinx.cinterop.*
import kvarc.graphics.vk.utils.VK_CHECK
import kvarc.utils.DisposableContainer
import vulkan.*

@ExperimentalUnsignedTypes
internal class Sync(
    private val _device: LogicalDevice,
    _swapchain: SwapChain
) : DisposableContainer() {

    var waitFences = ArrayList<VkFenceVar>().apply {
        for (i in 0 until _swapchain.imageBuffers.size)
            add(with(arena) { alloc<VkFenceVar>() })
    }
        private set

    // Semaphore used to ensures that image presentation is complete before starting to submit again
    private var presentCompleteSemaphore: VkSemaphoreVar = with(arena) { alloc() }

    // Semaphore used to ensures that all commands submitted have been finished before submitting the image to the queue
    private var renderCompleteSemaphore: VkSemaphoreVar = with(arena) { alloc() }
        private set

    val presentSemaphore by lazy {
        presentCompleteSemaphore.value
    }

    val presentSemaphorePtr by lazy {
        presentCompleteSemaphore.ptr
    }

    val renderSemaphore by lazy {
        presentCompleteSemaphore.value
    }

    val renderSemaphorePtr by lazy {
        presentCompleteSemaphore.ptr
    }

    init {

        for (i in 0 until waitFences.size) {
            memScoped {

                val fenceCreateInfo = alloc<VkFenceCreateInfo>().apply {
                    sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO
                    flags = VK_FENCE_CREATE_SIGNALED_BIT
                }

                if (!VK_CHECK(vkCreateFence(_device.device, fenceCreateInfo.ptr, null, waitFences[i].ptr)))
                    throw RuntimeException("Failed create fence")
            }
        }

        memScoped {

            val semaphoreCreateInfo = alloc<VkSemaphoreCreateInfo>().apply {
                sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO
                pNext = null
            }

            if (!VK_CHECK(
                    vkCreateSemaphore(
                        _device.device,
                        semaphoreCreateInfo.ptr,
                        null,
                        presentCompleteSemaphore.ptr
                    )
                )
            )
                throw  RuntimeException("Failed to create present semaphore")

            if (!VK_CHECK(
                    vkCreateSemaphore(
                        _device.device,
                        semaphoreCreateInfo.ptr,
                        null,
                        renderCompleteSemaphore.ptr
                    )
                )
            )
                throw  RuntimeException("Failed to create present semaphore")
        }

    }

    override fun dispose() {

        vkDestroySemaphore(_device.device, renderCompleteSemaphore.value, null)
        vkDestroySemaphore(_device.device, presentCompleteSemaphore.value, null)

        waitFences.forEach {
            vkDestroyFence(_device.device, it.value, null)
        }

        super.dispose()
    }

}
