/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc

import platform.posix.*
import platform.windows.*

import kotlinx.cinterop.*
import kotlin.native.concurrent.*

import kvarc.graphics.VideoMode
import kvarc.utils.DisposableContainer
import kvarc.global.sharedData

@ExperimentalUnsignedTypes
class WindowsSurface(private var isFullscreen: Boolean = false) : DisposableContainer() {

    var hwnd: HWND? = null
        private set

    private var _width = 800
    private var _height = 600

    private var maximized = false
    private var style: LONG = 0
    private var styleEx: LONG = 0
    private var rect = arena.alloc<LPRECTVar>().apply {
        pointed = arena.alloc()
    }

    private var originalVideoMode: VideoMode? = null

    fun initialize() {

        memScoped {

            val hInstance = GetModuleHandleW(null)
            val hBrush = CreateSolidBrush(0x00000000)

            val wc: WNDCLASSEXW = alloc()
            wc.cbSize = sizeOf<WNDCLASSEX>().convert()
            wc.style = (CS_HREDRAW or CS_VREDRAW or CS_OWNDC).convert()

            wc.lpfnWndProc = staticCFunction { hwnd, msg, wParam, lParam ->

                initRuntimeIfNeeded()

                when (msg.toInt()) {
                    WM_CLOSE -> {
                        val kotlinObject = DetachedObjectGraph<SharedCommonData>(sharedData.kotlinObject).attach()
                        val sharedData = kotlinObject.userdata!!.asStableRef<CommonData>().get()
                        if(sharedData.showWindowFullscreen){
                            sharedData.windowsSurface?.changeFullscreen(true)
                        }
                        sharedData.onTheRun = false
                        DestroyWindow(hwnd)
                    }
                    WM_DESTROY -> {
                        PostQuitMessage(0)
                    }
                    WM_LBUTTONDOWN -> {
                    }
                    WM_RBUTTONDOWN -> {
                    }
                    WM_MOUSEMOVE -> {
                        //val x: Int = (lParam and 0xFFFF).toInt()
                        //val y: Int = (lParam shr 16).toInt()
                    }
                    WM_KEYDOWN -> {
                        if (GetAsyncKeyState(VK_ESCAPE) != 0.toShort()) {
                            PostMessageW(hwnd, WM_CLOSE, 0, 0)
                        }
                    }
                    WM_SYSKEYDOWN -> {
                        if (wParam == VK_F4.toULong()) {
                            return@staticCFunction 1
                        }
                    }
                    else -> {
                        return@staticCFunction DefWindowProcW(hwnd, msg, wParam, lParam)
                    }
                }

                0
            }

            wc.cbClsExtra = 0
            wc.cbWndExtra = 0
            wc.hInstance = hInstance
            wc.hIcon = LoadIconW(null, IDI_APPLICATION)
            wc.hCursor = LoadCursorW(null, IDC_ARROW)
            wc.hbrBackground = hBrush
            wc.lpszMenuName = null
            wc.lpszClassName = "kvarc".wcstr.ptr
            wc.hIconSm = LoadIconW(null, IDI_APPLICATION)

            val failure: ATOM = 0u
            if (RegisterClassExW(wc.ptr) == failure) {
                throw RuntimeException("Failed to create native window!")
            }

            val kotlinObject = DetachedObjectGraph<SharedCommonData>(sharedData.kotlinObject).attach()
            val sharedData = kotlinObject.userdata!!.asStableRef<CommonData>().get()

            sharedData.windowsSurface = this@WindowsSurface

            hwnd = CreateWindowExW(
                WS_EX_CLIENTEDGE,
                "kvarc",
                "kvarc",
                WS_OVERLAPPEDWINDOW,
                CW_USEDEFAULT,
                CW_USEDEFAULT,
                _width,
                _height,
                null,
                null,
                hInstance,
                null
            )

            if (hwnd == null) {
                MessageBoxW(
                    null, "Failed to create native window!",
                    "kvarc", (MB_OK).convert()
                )
                throw RuntimeException("Failed to create native window!")
            }

            EnableMenuItem(
                GetSystemMenu(hwnd, FALSE), SC_CLOSE,
                (MF_BYCOMMAND or MF_DISABLED or MF_GRAYED).toUInt()
            )

            if (isFullscreen)
                changeFullscreen(true)

            ShowWindow(hwnd, SW_SHOWNORMAL)
            UpdateWindow(hwnd)

        }
    }

    private val videoModes by lazy {

        var modeIndex = 0u
        val modes = ArrayList<VideoMode>()

        memScoped {

            while (true) {

                val dm = alloc<DEVMODEW>()
                SecureZeroMemory!!(dm.ptr, sizeOf<DEVMODEW>().convert())
                dm.dmSize = sizeOf<DEVMODEW>().convert()

                if (EnumDisplaySettingsW(null, modeIndex, dm.ptr) == 0) break

                modeIndex++

                if (dm.dmBitsPerPel < 15u) continue

                val (red, green, blue) = splitBits(dm.dmBitsPerPel)
                modes.add(VideoMode(dm.dmPelsWidth, dm.dmPelsHeight, dm.dmDisplayFrequency, red, green, blue))

            }
        }

        modes

    }

    private var currentVideoMode: VideoMode? = null
        get() {

            if (field == null) {

                var mode: VideoMode? = null

                memScoped {

                    val dm = alloc<DEVMODEW>()
                    SecureZeroMemory!!(dm.ptr, sizeOf<DEVMODEW>().convert())
                    dm.dmSize = sizeOf<DEVMODEW>().convert()

                    EnumDisplaySettingsW(null, ENUM_CURRENT_SETTINGS, dm.ptr)

                    val (red, green, blue) = splitBits(dm.dmBitsPerPel)

                    mode = VideoMode(dm.dmPelsWidth, dm.dmPelsHeight, dm.dmDisplayFrequency, red, green, blue)
                }

                field = mode

            }

            return field
        }

    private fun changeVideoMode(needed: VideoMode) {

        val modes = videoModes

        //TODO replace the crutch with a normal search
        val closest = modes.minBy { videoMode ->
            sqrtf(
                powf((videoMode.width - needed.width).toInt().toFloat(), 2.0f) +
                        powf((videoMode.height - needed.height).toInt().toFloat(), 2.0f) +
                        powf((videoMode.refreshRate - needed.refreshRate).toInt().toFloat(), 2.0f) +
                        powf((videoMode.redBits - needed.redBits).toInt().toFloat(), 2.0f) +
                        powf((videoMode.greenBits - needed.greenBits).toInt().toFloat(), 2.0f) +
                        powf((videoMode.blueBits - needed.blueBits).toInt().toFloat(), 2.0f)
            ).toInt()
        }

        val current = currentVideoMode

        // The same video mode, no need to change
        if (current?.width == closest!!.width && current.height == closest.height
            && current.refreshRate == closest.refreshRate && current.redBits == closest.redBits
            && current.greenBits == closest.greenBits
            && current.blueBits == closest.blueBits
        ) return

        memScoped {

            val dm = alloc<DEVMODEW>()
            SecureZeroMemory!!(dm.ptr, sizeOf<DEVMODEW>().convert())
            dm.dmSize = sizeOf<DEVMODEW>().convert()

            dm.dmFields = (DM_PELSWIDTH or DM_PELSHEIGHT or DM_BITSPERPEL or DM_DISPLAYFREQUENCY).convert()
            dm.dmPelsWidth = closest.width
            dm.dmPelsHeight = closest.height
            dm.dmBitsPerPel = closest.redBits + closest.greenBits + closest.blueBits

            if (dm.dmBitsPerPel < 15u || dm.dmBitsPerPel >= 24u)
                dm.dmBitsPerPel = 32u

            if (ChangeDisplaySettingsExW(null, dm.ptr, null, CDS_FULLSCREEN, null) != DISP_CHANGE_SUCCESSFUL) {
                throw RuntimeException("Failed to change display settings")
            }

            currentVideoMode = closest

        }

    }

    private fun splitBits(bits: DWORD): Triple<DWORD, DWORD, DWORD> {

        val bpp = if (bits == 32u) 24u else bits

        var red = bpp / 3u
        var green = red
        val blue = red
        val delta = bpp - red * 3u
        if (delta >= 1u) green++
        if (delta == 2u) red++

        return Triple(red, green, blue)
    }

    fun center() {

        assert(hwnd != null)

        val screenWidth = GetSystemMetrics(SM_CXSCREEN)
        val screenHeight = GetSystemMetrics(SM_CYSCREEN)

        memScoped {

            val lpRect = alloc<LPRECTVar>().apply {
                pointed = alloc()
            }

            GetWindowRect(hwnd, lpRect.value)

            val nWidth = lpRect.value!!.pointed.right - lpRect.value!!.pointed.left
            val nHeight = lpRect.value!!.pointed.bottom - lpRect.value!!.pointed.top

            val nX = (screenWidth - nWidth) / 2
            val nY = (screenHeight - nHeight) / 2

            MoveWindow(hwnd, nX, nY, nWidth, nHeight, FALSE)

        }

    }

    fun changeFullscreen(fullscreen: Boolean) {

        assert(hwnd != null)

        memScoped {

            if (fullscreen) {

                // Save current window information.  We force the window into restored mode
                // before going showWindowFullscreen because Windows doesn't seem to hide the
                // taskbar if the window is in the maximized state

                originalVideoMode = currentVideoMode

                val placement = alloc<LPWINDOWPLACEMENTVar> {
                    pointed = alloc()
                }

                if (GetWindowPlacement(hwnd, placement.value) == 1) {
                    if (placement.pointed?.showCmd == SW_MAXIMIZE.toUInt()) {
                        maximized = true
                    }
                }

                if (maximized)
                    SendMessageW(hwnd, WM_SYSCOMMAND, SC_RESTORE, 0)

                style = GetWindowLongW(hwnd, GWL_STYLE)
                styleEx = GetWindowLongW(hwnd, GWL_EXSTYLE)

                GetWindowRect(hwnd, rect.value)

            }

            if (fullscreen) {

                changeVideoMode(VideoMode(800u, 600u, 59u, 8u, 8u, 8u))

                SetWindowLongW(hwnd, GWL_STYLE, style and (WS_CAPTION or WS_THICKFRAME).inv())

                SetWindowLongW(
                    hwnd,
                    GWL_EXSTYLE,
                    styleEx and (WS_EX_DLGMODALFRAME or WS_EX_WINDOWEDGE or WS_EX_CLIENTEDGE or WS_EX_STATICEDGE).inv()
                )


                val screenWidth = GetSystemMetrics(SM_CXSCREEN)
                val screenHeight = GetSystemMetrics(SM_CYSCREEN)

                SetWindowPos(
                    hwnd,
                    null,
                    0,
                    0,
                    screenWidth,
                    screenHeight,
                    SWP_NOZORDER.toUInt() or SWP_NOACTIVATE.toUInt() or SWP_FRAMECHANGED.toUInt()
                )

                isFullscreen = true

            } else {

                originalVideoMode?.let {

                    changeVideoMode(it)
                }

                // Reset original window style and size.  The multiple window size/moves
                // here are ugly, but if SetWindowPos() doesn't redraw, the taskbar won't be
                // repainted.  Better-looking methods welcome.
                SetWindowLongW(hwnd, GWL_STYLE, style)
                SetWindowLongW(hwnd, GWL_EXSTYLE, styleEx)

                // On restore, resize to the previous saved rect size.

                SetWindowPos(
                    hwnd,
                    null,
                    rect.pointed!!.left,
                    rect.pointed!!.top,
                    rect.pointed!!.right - rect.pointed!!.left,
                    rect.pointed!!.bottom - rect.pointed!!.top,
                    SWP_NOZORDER.toUInt() or SWP_NOACTIVATE.toUInt() or SWP_FRAMECHANGED.toUInt()
                )

                if (maximized)
                    SendMessageW(hwnd, WM_SYSCOMMAND, SC_MAXIMIZE, 0)

                isFullscreen = false
            }

        }

    }

    fun messageLoop() {

        memScoped {

            val msg: MSG = alloc()

            while (GetMessageW(msg.ptr, null, 0u, 0u) > 0) {
                TranslateMessage(msg.ptr)
                DispatchMessageW(msg.ptr)
            }

        }

    }

}