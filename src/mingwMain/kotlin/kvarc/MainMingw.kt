/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc

import platform.posix.*
import platform.windows.*

import kotlinx.cinterop.*
import kotlin.native.concurrent.DetachedObjectGraph
import kotlin.native.concurrent.attach

import kvarc.utils.PlatformEnum
import kvarc.utils.ensureUnixCallResult
import kvarc.utils.logError
import kvarc.global.sharedData
import kvarc.graphics.Renderer
import kvarc.graphics.vk.utils.VK_CHECK
import vulkan.*

@ExperimentalUnsignedTypes
internal actual object Platform {

    actual fun initialize() {

        try {

            val arena = Arena()

            val semaphore = arena.alloc<sem_tVar>()
            sem_init(semaphore.ptr, 0, 0)

            val commonData = CommonData(semaphore)
            commonData.showWindowFullscreen = false
            val hInstance = platform.windows.GetModuleHandleW(null)
            commonData.hInstance = hInstance!!
            val commonDataStableRef = StableRef.create(commonData)

            sharedData.kotlinObject = DetachedObjectGraph {
                SharedCommonData(commonDataStableRef.asCPointer())
            }.asCPointer()

            memScoped {

                val winThread = alloc<pthread_tVar>()

                // It lies about redundant lambda arrow
                pthread_create(winThread.ptr, null, staticCFunction { _: COpaquePointer? ->

                    initRuntimeIfNeeded()

                    val kotlinObject = DetachedObjectGraph<SharedCommonData>(sharedData.kotlinObject).attach()
                    val data = kotlinObject.userdata!!.asStableRef<CommonData>().get()

                    val win = WindowsSurface(data.showWindowFullscreen)
                    win.initialize()

                    if (!data.showWindowFullscreen && data.showWindowCentered)
                        win.center()

                    @Suppress("UNCHECKED_CAST")
                    data.hwnd = win.hwnd!!

                    sem_post(data.semaphore.ptr)

                    win.messageLoop()
                    win.dispose()

                    null as COpaquePointer? //keep it lies not needed
                }, null)
                    .ensureUnixCallResult("pthread_create")

                val vkThread = alloc<pthread_tVar>()

                // It lies about redundant lambda arrow
                pthread_create(vkThread.ptr, null, staticCFunction { _: COpaquePointer? ->

                    initRuntimeIfNeeded()

                    val kotlinObject = DetachedObjectGraph<SharedCommonData>(sharedData.kotlinObject).attach()
                    val data = kotlinObject.userdata!!.asStableRef<CommonData>().get()
                    sem_wait(data.semaphore.ptr)

                    var renderer: Renderer? = null
                    try {

                        renderer = Renderer(kotlinObject)
                        renderer.initialize()
                        while (data.onTheRun) {
                            renderer.draw()
                        }

                    } catch (exc: Exception) {
                        logError(exc.message ?: "Failed to create renderer")
                    }
                    finally {
                        renderer?.dispose()
                    }

                    null as COpaquePointer? //keep it lies not needed

                }, null)
                    .ensureUnixCallResult("pthread_create")

                pthread_join(vkThread.value, null).ensureUnixCallResult("pthread_join")
                pthread_join(winThread.value, null).ensureUnixCallResult("pthread_join")

            }

            sem_destroy(semaphore.ptr)
            commonDataStableRef.dispose()
            arena.clear()

        } catch (ex: Exception) {
            logError("Failed to start with exception: ${ex.message}")
        }

    }

    actual val type = PlatformEnum.WINDOWS
    actual val VK_KHR_PLATFORM_SURFACE_EXTENSION_NAME = VK_KHR_WIN32_SURFACE_EXTENSION_NAME

    @Suppress("UNCHECKED_CAST")
    actual fun  createSurface(instance: VkInstance, surfaceVar: VkSurfaceKHRVar, shared: SharedCommonData) {

        memScoped {

            val sharedData = shared.userdata!!.asStableRef<CommonData>().get()

            val surfaceInfo = alloc<VkWin32SurfaceCreateInfoKHR> {
                sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR
                pNext = null
                flags = 0u
                hinstance = sharedData.hInstance!! as vulkan.HINSTANCE
                hwnd = sharedData.hwnd!! as vulkan.HWND

            }

            if (!VK_CHECK(vkCreateWin32SurfaceKHR(instance, surfaceInfo.ptr, null, surfaceVar.ptr))) {
                throw RuntimeException("Failed to create surface.")
            }

        }

    }

}

