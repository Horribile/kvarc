/*
 * Copyright (C) 2018 Igor Kushnarev
 *
 * This file is part of kvarc.
 *
 * kvarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kvarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package kvarc

import platform.posix.*
import platform.windows.*
import kotlinx.cinterop.*

/**
 * Data shared between threads
 */
internal class CommonData @ExperimentalUnsignedTypes constructor(
    val semaphore: sem_tVar,
    var hInstance: HINSTANCE? = null,
    var hwnd: HWND? = null,
    var showWindowCentered: Boolean = true,
    var showWindowFullscreen: Boolean = false,
    var onTheRun: Boolean = true,
    var windowsSurface: WindowsSurface? = null
)

internal data class SharedCommonData(val userdata: COpaquePointer?)
